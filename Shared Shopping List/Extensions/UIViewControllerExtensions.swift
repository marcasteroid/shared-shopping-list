//
//  UIViewControllerExtensions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit
import Firebase

extension UIViewController {
    
    // MARK: - Firebase authentication error
    
    // Handle Firebase authentication and user creation error
    func handleFirebaseAuthError(error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            showAlert(alertControllerTitle: Message.StandardLogin.Alert.Error.title, alertControllerMessage: errorCode.errorMessage, alertActionTitle: Message.StandardLogin.Alert.Actions.ok)
        }
    }
    
    // MARK: - User authentication UI
    func handleUserAuthenticationError(forError error: String) {
        switch error {
        // Email text field error
        case UserUI.Authentication.Error.emailTextFieldError:
            showAlert(alertControllerTitle: Message.StandardLogin.Alert.Email.title, alertControllerMessage: Message.StandardLogin.Alert.Email.message, alertActionTitle: Message.StandardLogin.Alert.Actions.ok)
        // Password text field error
        case UserUI.Authentication.Error.passwordTextFieldError:
            showAlert(alertControllerTitle: Message.StandardLogin.Alert.Password.title, alertControllerMessage: Message.StandardLogin.Alert.Password.message, alertActionTitle: Message.StandardLogin.Alert.Actions.ok)
        // Unknown error
        default:
            showAlert(alertControllerTitle: Message.StandardLogin.Alert.Unknown.title, alertControllerMessage: Message.StandardLogin.Alert.Unknown.message, alertActionTitle: Message.StandardLogin.Alert.Actions.ok)
        }
    }
    
    // MARK: - Firebare user creation error
    func handleFirebaseUserCreationError(error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            showAlert(alertControllerTitle: Message.CreateUser.Alert.Error.title, alertControllerMessage: errorCode.errorMessage, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        }
    }
    
    // MARK: - User creation UI
    func handleUserCreationError(forError error: String) {
        switch error {
        // User name text field error
        case UserUI.Create.Error.usernameTextFieldError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.Username.title, alertControllerMessage: Message.CreateUser.Alert.Username.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Email text field error
        case UserUI.Create.Error.emailTextFieldError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.Email.title, alertControllerMessage: Message.CreateUser.Alert.Email.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Country text field error
        case UserUI.Create.Error.countryTextFieldError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.Country.title, alertControllerMessage: Message.CreateUser.Alert.Country.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Country not found
        case UserUI.Create.Error.countryNotFoundError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.CountryNotFound.title, alertControllerMessage: Message.CreateUser.Alert.CountryNotFound.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Phone number text field error
        case UserUI.Create.Error.phoneNumberTextFieldError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.PhoneNumber.title, alertControllerMessage: Message.CreateUser.Alert.PhoneNumber.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Phone number badly formatted
        case UserUI.Create.Error.phoneNumberBadlyFormattedError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.PhoneNumberBadlyFormatted.title, alertControllerMessage: Message.CreateUser.Alert.PhoneNumberBadlyFormatted.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Password text field error
        case UserUI.Create.Error.passwordTextFieldError:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.Password.title, alertControllerMessage: Message.CreateUser.Alert.Password.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Unknown error
        default:
            showAlert(alertControllerTitle: Message.CreateUser.Alert.Unknown.title, alertControllerMessage: Message.CreateUser.Alert.Unknown.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        }
    }
    
    // MARK: - Shopping list creation error
    func handleShoppingListCreationError(forError error: String) {
        switch error {
        // Shopping list name error
        case ShoppingListCreation.CreateNewShoppingList.Error.shoppingListNameTextFieldError:
            showAlert(alertControllerTitle: Message.ShoppingListCreation.Alert.Name.title, alertControllerMessage: Message.ShoppingListCreation.Alert.Name.message, alertActionTitle: Message.ShoppingListCreation.Alert.Actions.ok)
        // Shopping list tags error
        case ShoppingListCreation.CreateNewShoppingList.Error.shoppingListTagsTextFieldError:
            showAlert(alertControllerTitle: Message.ShoppingListCreation.Alert.Tags.title, alertControllerMessage: Message.ShoppingListCreation.Alert.Tags.message, alertActionTitle: Message.ShoppingListCreation.Alert.Actions.ok)
            // Shopping list new item error
        case ShoppingListCreation.CreateNewShoppingList.Error.shoppingListItemNameTextFieldError:
            showAlert(alertControllerTitle: Message.ShoppingListCreation.Alert.ItemName.title, alertControllerMessage: Message.ShoppingListCreation.Alert.ItemName.message, alertActionTitle: Message.ShoppingListCreation.Alert.Actions.ok)
        // Unknown error
        default:
            showAlert(alertControllerTitle: Message.ShoppingListCreation.Alert.Unknown.title, alertControllerMessage: Message.ShoppingListCreation.Alert.Unknown.message, alertActionTitle: Message.ShoppingListCreation.Alert.Actions.ok)
        }
    }
    
    // MARK: - Group creation error
    func handleGroupCreationError(forError error: String) {
        switch error {
            // Group name error
        case GroupCreation.CreateNewGroup.Error.groupNameTextFieldError:
            showAlert(alertControllerTitle: Message.GroupCreation.Alert.Title.title, alertControllerMessage: Message.GroupCreation.Alert.Title.message, alertActionTitle: Message.GroupCreation.Alert.Actions.ok)
        default:
            showAlert(alertControllerTitle: Message.GroupCreation.Alert.Unknown.title, alertControllerMessage: Message.GroupCreation.Alert.Unknown.message, alertActionTitle: Message.GroupCreation.Alert.Actions.ok)
        }
    }
    
    func handleEditUserProfileError(forError error: String) {
        switch error {
        // User first name error
        case UserUI.Edit.Error.firstNameTextFieldError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.FirstName.title, alertControllerMessage: Message.EditUser.Alert.FirstName.message, alertActionTitle: Message.EditUser.Alert.Actions.ok)
        // User last name error
        case UserUI.Edit.Error.lastNameTextFieldError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.LastName.title, alertControllerMessage: Message.EditUser.Alert.LastName.message, alertActionTitle: Message.EditUser.Alert.Actions.ok)
        // Country text field error
        case UserUI.Edit.Error.countryTextFieldError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.Country.title, alertControllerMessage: Message.EditUser.Alert.Country.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Country not found
        case UserUI.Edit.Error.countryNotFoundError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.CountryNotFound.title, alertControllerMessage: Message.EditUser.Alert.CountryNotFound.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Phone number text field error
        case UserUI.Edit.Error.phoneNumberTextFieldError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.PhoneNumber.title, alertControllerMessage: Message.EditUser.Alert.PhoneNumber.message, alertActionTitle: Message.CreateUser.Alert.Actions.ok)
        // Phone number badly formatted
        case UserUI.Edit.Error.phoneNumberBadlyFormattedError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.PhoneNumberBadlyFormatted.title, alertControllerMessage: Message.EditUser.Alert.PhoneNumberBadlyFormatted.message, alertActionTitle: Message.EditUser.Alert.Actions.ok)
        // Age missing error
        case UserUI.Edit.Error.ageTextFieldError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.AgeMissing.title, alertControllerMessage: Message.EditUser.Alert.AgeMissing.message, alertActionTitle: Message.EditUser.Alert.Actions.ok)
        // Age contains characters error
        case UserUI.Edit.Error.ageTextFieldContainsCharactersError:
            showAlert(alertControllerTitle: Message.EditUser.Alert.AgeContainsCharacters.title, alertControllerMessage: Message.EditUser.Alert.AgeContainsCharacters.message, alertActionTitle: Message.EditUser.Alert.Actions.ok)
        default:
            showAlert(alertControllerTitle: Message.EditUser.Alert.Unknown.title, alertControllerMessage: Message.EditUser.Alert.Unknown.message, alertActionTitle: Message.EditUser.Alert.Actions.ok)
        }
    }
    
    // MARK: - Utility functions
    
    // Show alert
    func showAlert(alertControllerTitle: String, alertControllerMessage: String, alertActionTitle: String) {
        // Initialize alert controller
        let alertController = UIAlertController(title: alertControllerTitle, message: alertControllerMessage, preferredStyle: .alert)
        // Initialize action
        let action = UIAlertAction(title: alertActionTitle, style: .default, handler: nil)
        // Configure alert controller
        alertController.addAction(action)
        // Present alert controller
        present(alertController, animated: true, completion: nil)
    }
    
}
