//
//  KeyboardBoundView.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 25/06/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

extension UIView {
    
    // Bind to keyboard
    func bindToKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(UIView.keyboardWillChange(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Keyboard will change
    @objc func keyboardWillChange(_ notification: Notification) {
        // Animation duration
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        // Animation curve
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        // Current frame
        let currentFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        // Target frame
        let targetFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        // Delta for y position
        let deltaY = targetFrame.origin.y - currentFrame.origin.y
        
        // Start animation
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY
        }, completion: {(true) in
            self.layoutIfNeeded()
        })
    }
}
