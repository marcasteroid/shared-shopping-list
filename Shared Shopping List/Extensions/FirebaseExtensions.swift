//
//  FirebaseExtensions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase

// MARK: - Authentication errors

extension AuthErrorCode  {
    
    // Error messages
    var errorMessage: String {
        switch self {
        // Email already in use
        case .emailAlreadyInUse:
            return FirebaseErrorMessage.CreateUser.emailAlreadyInUse
        // Weak password
        case .weakPassword:
            return FirebaseErrorMessage.CreateUser.weakPassword
        // User not found
        case .userNotFound:
            return FirebaseErrorMessage.Authentication.userNotFound
        // User disabled
        case .userDisabled:
            return FirebaseErrorMessage.Authentication.userDisabled
        // Invalid email, invalid sender, invalid recipient email
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
            return FirebaseErrorMessage.Authentication.invalidEmail
        // Wrong password
        case .wrongPassword:
            return FirebaseErrorMessage.Authentication.wrongPassword
        // Unknown error
        default:
            return FirebaseErrorMessage.Unkwnown.unknown
        }
    }
}
