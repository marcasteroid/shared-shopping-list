//
//  UITextFieldExtensions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// UITextField extension

import Foundation
import UIKit

extension UITextField {
    
    // Clear text field
    func clear() {
        self.text = Common.Value.empty
    }
}
