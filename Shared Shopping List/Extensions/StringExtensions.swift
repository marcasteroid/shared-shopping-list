//
//  StringExtensions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 20/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

extension String {
    
    // Return false if the string has no character, true otherwise
    var isNotEmpty: Bool {
        return !isEmpty
    }
    
    // Check if the string is a valid phone number
    var isNotValid: Bool {
        let phoneRegex = Formatter.PhoneNumber.regex
        let valid = NSPredicate(format: Formatter.PhoneNumber.predicate, phoneRegex).evaluate(with: self)
        return valid
    }
    
    // Check if contains only decimal digits
    var containsOnlyDigits: Bool {
        return self.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil
    }
    
    // Capitalize only first letter
    func capitalizeFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizeFirstLetter()
    }
    
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
}
