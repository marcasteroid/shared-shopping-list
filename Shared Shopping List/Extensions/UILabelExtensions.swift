//
//  UILabelExtensions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 09/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

public extension UILabel {
    
    // Initialize a UILabel with text
    convenience init(text: String?) {
        self.init()
        self.text = text
    }
    
    // Make font regular and bold
    func makeFontStyleRegularAndBold(headText: String, tailText: String) {
        let regularFont = UIFont(name: UI.Font.regular, size: UI.Font.size)
        let demiBoldFont = UIFont(name: UI.Font.demiBold, size: UI.Font.size)
        let regularAttribute = [NSAttributedString.Key.font: regularFont]
        let demiBoldAttribute = [NSAttributedString.Key.font: demiBoldFont]

        let regularText = NSAttributedString(string: headText + Common.Value.singleSpace, attributes: regularAttribute)
        let demiBoldText = NSAttributedString(string: tailText, attributes: demiBoldAttribute)
        let title = NSMutableAttributedString()
        title.append(regularText)
        title.append(demiBoldText)
        attributedText = title
    }
    
    // Clear text
    func clear() {
        self.text = Common.Value.empty
    }
    
}
