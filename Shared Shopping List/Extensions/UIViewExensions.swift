//
//  UIViewExensions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 28/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

protocol MultiTappableDelegate: class {
    func doubleTapDetected(in view: MultiTappable)
    func singleTapDetected(in view: MultiTappable)
}

class ThreadSafeValue<T> {
    private var _value: T
    private lazy var semaphore = DispatchSemaphore(value: 1)
    
    init(value: T) { _value = value }
    
    var value: T {
        get {
            semaphore.signal()
            defer { semaphore.wait() }
            return _value
        }
        
        set(value) {
            semaphore.signal()
            defer { semaphore.wait() }
            _value = value
        }
    }
}

protocol MultiTappable: UIView {
    var multiTapDelegate: MultiTappableDelegate? { get set }
    var tapCounter: ThreadSafeValue<Int> { get set }
}

extension MultiTappable {
    
    // MARK: - Properties
    
    func initMultiTap() {
        if let delegate = self as? MultiTappableDelegate { multiTapDelegate = delegate }
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIView.multiTapActionHandler))
        addGestureRecognizer(tap)
    }
    
    func deinitMultiTap() {
        self.gestureRecognizers?.removeAll()
    }
    
    func multiTapAction() {
        if tapCounter.value == 0 {
            DispatchQueue.global(qos: .utility).async {
                usleep(250_000)
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    if self.tapCounter.value > 1 {
                        self.multiTapDelegate?.doubleTapDetected(in: self)
                    } else {
                        self.multiTapDelegate?.singleTapDetected(in: self)
                    }
                    self.tapCounter.value = 0
                }
            }
        }
        tapCounter.value += 1
    }
}

private extension UIView {
    @objc func multiTapActionHandler() {
        if let tappable = self as? MultiTappable { tappable.multiTapAction() }
    }
}
