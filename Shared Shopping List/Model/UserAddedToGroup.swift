//
//  UserAddedToGroup.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/07/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct UserAddedToGroup {

    // MARK: - Properties
    
    // First name
    private (set) public var firstName: String!
    // Last name
    private (set) public var lastName: String!
    // Phone number
    private (set) public var phoneNumber: String!
    
    // MARK: - Initializers
    
    // Default initializer
    init(firstName: String, lastName: String, phoneNumber: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.phoneNumber = phoneNumber
    }
}
