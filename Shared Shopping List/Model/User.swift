//
//  User.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

// ADD DESCRIPTION

import Foundation

struct User {
    // MARK: - Properties
    
    // Username
    var username: String
    // Email
    var email: String
    // Password
    var password: String
    // Phone number
    var phoneNumber: String
    // Phone number with prefix
    var phoneNumberWithPrefix: String
    // Image URL
    var imageURL: String
    // ID
    var id: String
    // First name
    var firstName: String
    // Last name
    var lastName: String
    // Age
    var age: String
    
    static func modelToData(user: User) -> [String: Any] {
        let data: [String: Any] = [
            FirebaseCollection.Users.username : user.username,
            FirebaseCollection.Users.emailAddress : user.email,
            FirebaseCollection.Users.phoneNumber : user.phoneNumber,
            FirebaseCollection.Users.phoneNumberWithPrefix : user.phoneNumberWithPrefix,
            FirebaseCollection.Users.imageURL : user.imageURL,
            FirebaseCollection.Users.id : user.id
        ]
        
        return data
    }
}
