//
//  Tag.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 06/06/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

class Tag: Equatable {
    static func == (lhs: Tag, rhs: Tag) -> Bool {
        return lhs.name == rhs.name
    }
    
    var name: String?
    var selected = false
}
