//
//  Group.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/07/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase

class Group {
    
    // MARK:-  Properties
    
    // Document identifier
    private (set) public var documentID: String!
    // Admin identifier
    private (set) public var adminID: String!
    // Name
    private (set) public var name: String!
    // Number of members
    private (set) public var membersCount: Int!
    // Members
    private (set) public var members: [String]!
    // Timestamp
    private (set) public var timestamp: Timestamp
    // Array of shopping list IDs
    private (set) public var shoppingListsID: [String]!
    // Label
    private (set) public var label: String!
    
    // Mark:- Initializer
    init(documentID: String, adminID: String, name: String, membersCount: Int, members: [String], timestamp: Timestamp = Timestamp(), shoppingListsID: [String], label: String) {
        self.documentID = documentID
        self.adminID = adminID
        self.name = name
        self.membersCount = membersCount
        self.members = members
        self.timestamp = timestamp
        self.shoppingListsID = shoppingListsID
        self.label = label
    }
    
    init(data: [String: Any]) {
        self.documentID = data[FirebaseCollection.Groups.documentID] as? String ?? Common.Value.empty
        self.adminID = data[FirebaseCollection.Groups.adminID] as? String ?? Common.Value.empty
        self.name = data[FirebaseCollection.Groups.name] as? String ?? Common.Value.empty
        self.membersCount = data[FirebaseCollection.Groups.membersCount] as? Int ?? 0
        self.members = data[FirebaseCollection.Groups.members] as? [String] ?? []
        self.timestamp = data[FirebaseCollection.Groups.timestamp] as? Timestamp ?? Timestamp()
        self.shoppingListsID = data[FirebaseCollection.Groups.shoppingListsID] as? [String] ?? []
        self.label = data[FirebaseCollection.Groups.label] as? String ?? Common.Value.empty
    }
    
    // MARK: - Utilities
    
    // Convert an object to an array
    static func modelToData(group: Group) -> [String: Any] {
        let data: [String: Any] = [
            FirebaseCollection.Groups.documentID: group.documentID!,
            FirebaseCollection.Groups.adminID: group.adminID!,
            FirebaseCollection.Groups.name: group.name!,
            FirebaseCollection.Groups.membersCount: group.membersCount!,
            FirebaseCollection.Groups.members: group.members!,
            FirebaseCollection.Groups.timestamp: group.timestamp,
            FirebaseCollection.Groups.shoppingListsID: group.shoppingListsID!
        ]
        return data
    }
    
    // Parse data
    class func parseData(snapshot: QuerySnapshot?) -> [Group] {
        var groups = [Group]()
        
        guard let snapshot = snapshot else { return groups }
        
        for document in snapshot.documents {
            let data = document.data()
            
            let documentID = document.documentID
            let adminID = data[FirebaseCollection.Groups.adminID] as? String ?? Common.Value.empty
            let name = data[FirebaseCollection.Groups.label] as? String ?? Common.Value.empty
            let membersCount = data[FirebaseCollection.Groups.membersCount] as? Int ?? 0
            let members = data[FirebaseCollection.Groups.members] as? [String] ?? []
            let timestamp = data[FirebaseCollection.Groups.timestamp] as? Timestamp ?? Timestamp()
            let shoppingListsID = data[FirebaseCollection.Groups.shoppingListsID] as? [String] ?? []
            let label = data[FirebaseCollection.Groups.label] as? String ?? Common.Value.empty
            
            let newGroup = Group(documentID: documentID, adminID: adminID, name: name, membersCount: membersCount, members: members, timestamp: timestamp, shoppingListsID: shoppingListsID, label: label)
            groups.append(newGroup)
        }
        return groups
    }
}
