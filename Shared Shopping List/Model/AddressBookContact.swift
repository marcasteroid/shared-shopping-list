//
//  AddressBookContact.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 09/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

struct AddressBookContact {
    // First name
    private(set) public var firstName: String
    // Last name
    private(set) public var lastName: String
    // Image
    private(set) public var image: UIImage
    // Has contact image
    private(set) public var hasContactImage: Bool
    
    // MARK: - Initializers
    init(firstName: String, familyName: String, image: UIImage, hasContactImage: Bool = false) {
        self.firstName = firstName
        self.lastName = familyName
        self.image = image
        self.hasContactImage = hasContactImage
    }
}
