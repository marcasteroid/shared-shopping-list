//
//  UserGroup.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Describe an user added to a group

import Foundation
import UIKit

struct UserGroup: Comparable {

    private (set) public var image: UIImage?
    private (set) public var fullname: String
    
    init(image: UIImage?, fullname: String) {
        self.image = image
        self.fullname = fullname
    }
    
    static func == (lhs: UserGroup, rhs: UserGroup) -> Bool {
        return lhs.fullname == rhs.fullname
    }
    
    static func < (lhs: UserGroup, rhs: UserGroup) -> Bool {
        return lhs.fullname < rhs.fullname
    }
}
