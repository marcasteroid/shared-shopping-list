//
//  Country.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Country: Object, Decodable {
    // MARK: - Properties
    // Object ID
    @objc dynamic private (set) public var id: String! = Common.Value.empty
    // Name
    @objc dynamic private (set) public var name: String! = Common.Value.empty
    // Code
    @objc dynamic private (set) public var code: String! = Common.Value.empty
    // ISO
    @objc dynamic private (set) public var iso: String! = Common.Value.empty
    
    // Primary key
    override class func primaryKey() -> String {
        return RealmDatabase.Attributes.primaryKey
    }
    
    // Indexed properties
    override class func indexedProperties() -> [String] {
        return []
    }
    
    // MARK: - Initializer
    convenience init(name: String, code: String, iso: String) {
        self.init()
        // Generate random id
        self.id = UUID().uuidString.lowercased()
        // Name
        self.name = name
        // Code
        self.code = code
        // Iso
        self.iso = iso
    }
    
    static func addCountryToRealm(name: String, code: String, iso: String) {
        
        // Create country object
        let country = Country(name: name, code: code, iso: iso)
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(country)
                try realm.commitWrite()
            }
        } catch let error {
            debugPrint("Error adding Country code to realm: \(error.localizedDescription)")
        }
    }
}
