//
//  Item.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 17/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase

/*
struct Item {
    
    // MARK: - Properties
    private (set) public var name: String!
    private (set) public var shared: Bool!
    private (set) public var timeStamp: Date!
    private (set) public var documentID: String!
    private (set) public var shoppingListDocumentID: String!
    
    init(name: String, shared: Bool, timeStamp: Date, documentID: String, shoppingListDocumentID: String) {
        self.name = name
        self.shared = shared
        self.timeStamp = timeStamp
        self.documentID = documentID
        self.shoppingListDocumentID = shoppingListDocumentID
    }
}
*/

class Item {
    
    // MARK: - Properties
    private (set) public var name: String!
    private (set) public var done: Bool!
    private (set) public var timestamp: Timestamp
    private (set) public var shoppingListDocumentID: String!
    private (set) public var documentID: String!
    
    // MARK: - Initializers
    init(name: String, done: Bool, timestamp: Timestamp = Timestamp(), shoppingListDocumentID: String, documentID: String) {
        self.name = name
        self.done = done
        self.timestamp = timestamp
        self.shoppingListDocumentID = shoppingListDocumentID
        self.documentID = documentID
    }
    
    // MARK: - Utilities
    class func parseData(snapshot: QuerySnapshot?) -> [Item] {
        var items = [Item]()
        
        guard let snapshot = snapshot else { return items }
        for document in snapshot.documents {
            let data = document.data()
            let name = data[FirebaseCollection.Items.name] as? String ?? Common.Value.empty
            let timestamp = data[FirebaseCollection.Items.timestamp] as? Timestamp ?? Timestamp()
            let done = data[FirebaseCollection.Items.done] as? Bool ?? false
            let shoppingListDocumentID = data[FirebaseCollection.Items.shoppingListID] as? String ?? Common.Value.empty
            
            let documentID = document.documentID
            
            let newItem = Item(name: name, done: done, timestamp: timestamp, shoppingListDocumentID: shoppingListDocumentID, documentID: documentID)
            
            items.append(newItem)
        }
        
        return items
    }
}
