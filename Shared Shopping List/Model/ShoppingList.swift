//
//  ShoppingList.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 08/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase

struct ShoppingList {

    // MARK: - Properties
    // Name
    private (set) public var name: String!
    // Number of items
    private (set) public var numberOfItems: Int!
    // Shared
    private (set) public var shared: Bool!
    // Completed
    private (set) public var completed: Bool!
    // Saved
    private (set) public var saved: Bool!
    // Tags
    private (set) public var tags: String!
    // Timestamp
    private (set) public var timestamp: Timestamp
    // Date complete
    private (set) public var dateComplete: Timestamp
    // Document id
    private (set) public var documentID: String!
    // Admin id
    private (set) public var adminID: String!
    
    // MARK: - Initializer
    
    init(name: String, numberOfItems: Int, shared: Bool, completed: Bool, saved: Bool, tags: String, timestamp: Timestamp = Timestamp(), dateCompleted: Timestamp, documentID: String, adminID: String) {
        self.name = name
        self.numberOfItems = numberOfItems
        self.shared = shared
        self.completed = completed
        self.saved = saved
        self.tags = tags
        self.timestamp = timestamp
        self.dateComplete = dateCompleted
        self.documentID = documentID
        self.adminID = adminID
    }
    
    init(data: [String: Any]) {
        self.name = data[FirebaseCollection.ShoppingLists.name] as? String ?? Common.Value.empty
        self.timestamp = data[FirebaseCollection.ShoppingLists.timestamp] as? Timestamp ?? Timestamp()
        self.dateComplete = data[FirebaseCollection.ShoppingLists.dateCompleted] as? Timestamp ?? Timestamp()
        self.numberOfItems = data[FirebaseCollection.ShoppingLists.numberOfItems] as? Int ?? 0
        self.tags = data[FirebaseCollection.ShoppingLists.tags] as? String ?? Common.Value.empty
        self.shared = data[FirebaseCollection.ShoppingLists.shared] as? Bool ?? false
        self.completed = data[FirebaseCollection.ShoppingLists.completed] as? Bool ?? false
        self.saved = data[FirebaseCollection.ShoppingLists.saved] as? Bool ?? false
        self.adminID = data[FirebaseCollection.ShoppingLists.adminID] as? String ?? Common.Value.empty
        self.documentID = data[FirebaseCollection.ShoppingLists.id] as? String ?? Common.Value.empty
    }

    // MARK: - Utils
    
    // Convert and object to an array
    static func modelToData(shoppingList: ShoppingList) -> [String: Any] {
        let data: [String: Any] = [
            FirebaseCollection.ShoppingLists.name: shoppingList.name!,
            FirebaseCollection.ShoppingLists.timestamp: shoppingList.timestamp,
            FirebaseCollection.ShoppingLists.dateCompleted: shoppingList.completed!,
            FirebaseCollection.ShoppingLists.numberOfItems: shoppingList.numberOfItems!,
            FirebaseCollection.ShoppingLists.tags: shoppingList.tags!,
            FirebaseCollection.ShoppingLists.shared: shoppingList.shared!,
            FirebaseCollection.ShoppingLists.completed: shoppingList.completed!,
            FirebaseCollection.ShoppingLists.saved: shoppingList.saved!,
            FirebaseCollection.ShoppingLists.adminID: shoppingList.adminID!,
            FirebaseCollection.ShoppingLists.id: shoppingList.documentID!
        ]
        return data
    }
    
    // Parse data
    static func parseData(snapshot: QuerySnapshot?) -> [ShoppingList] {
        var shoppingLists = [ShoppingList]()
        
        guard let snapshot = snapshot else { return shoppingLists }
        
        for document in snapshot.documents {
            let data = document.data()
            
            let name = data[FirebaseCollection.ShoppingLists.name] as? String ?? Common.Value.empty
            let timestamp = data[FirebaseCollection.ShoppingLists.timestamp] as? Timestamp ?? Timestamp()
            let dateComplete = data[FirebaseCollection.ShoppingLists.dateCompleted] as? Timestamp ?? Timestamp()
            let numberOfItems = data[FirebaseCollection.ShoppingLists.numberOfItems] as? Int ?? 0
            let tags = data[FirebaseCollection.ShoppingLists.tags] as? String ?? Common.Value.empty
            let shared = data[FirebaseCollection.ShoppingLists.shared] as? Bool ?? false
            let completed = data[FirebaseCollection.ShoppingLists.completed] as? Bool ?? false
            let saved = data[FirebaseCollection.ShoppingLists.saved] as? Bool ?? false
            let adminID = data [FirebaseCollection.ShoppingLists.adminID] as? String ?? Common.Value.empty
            let documentID = document.documentID
            
            let shoppingList = ShoppingList(name: name, numberOfItems: numberOfItems, shared: shared, completed: completed, saved: saved, tags: tags, timestamp: timestamp, dateCompleted: dateComplete, documentID: documentID, adminID: adminID)
            shoppingLists.append(shoppingList)
        }
        
        return shoppingLists
    }
}

extension ShoppingList: Equatable {
    static func ==(lhs: ShoppingList, rhs: ShoppingList) -> Bool {
        return lhs.documentID == rhs.documentID
    }
}
