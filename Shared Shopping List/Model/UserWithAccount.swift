//
//  UserWithAccount.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 30/06/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

class UserWithAccount {
    // MARK: - Properties
    
    // User id
    private var _id: String
    // Username
    private var _username: String
    // Email
    private var _email: String
    // Phone number
    private var _phoneNumber: String
    
    // MARK: - Getters
    
    var id: String {
        return _id
    }
    
    var username: String {
        return _username
    }
    
    var email: String {
        return _email
    }
    
    var phoneNumber: String {
        return _phoneNumber
    }
    
    init(id: String, username: String, email: String, phoneNumber: String) {
        self._id = id
        self._username = username
        self._email = email
        self._phoneNumber = phoneNumber
    }
}
