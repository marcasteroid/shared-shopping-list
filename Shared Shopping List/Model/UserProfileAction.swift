//
//  UserProfileAction.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

struct UserProfileAction {
    
    // MARK: - Properties
    
    // Name of the action
    private (set) public var name: String!
    // Image for the action
    private (set) public var imageName: String!
    
    // MARK: - Initializers
    
    // Default initializer
    init(name: String, imageName: String) {
        self.name = name
        self.imageName = imageName
    }
}

