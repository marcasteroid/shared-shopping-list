//
//  SavedShoppingList.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 10/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase

class SavedShoppingList {
    
    // MARK: - Properties
    // Name
    private (set) public var name: String!
    // Number of items
    private (set) public var numberOfItems: Int!
    // Shared
    private (set) public var shared: Bool!
    // Completed
    private (set) public var completed: Bool!
    // Saved
    private (set) public var saved: Bool!
    // Tags
    private (set) public var tags: String!
    // Timestamp
    private (set) public var timestamp: Timestamp
    // Date complete
    private (set) public var dateComplete: Timestamp
    // Document id
    private (set) public var documentID: String!
    
    // MARK: - Initializer
    init(name: String, numberOfItems: Int, shared: Bool, completed: Bool, saved: Bool, tags: String, timestamp: Timestamp = Timestamp(), dateComplete: Timestamp, documentID: String) {
        self.name = name
        self.numberOfItems = numberOfItems
        self.shared = shared
        self.completed = completed
        self.saved = saved
        self.tags = tags
        self.timestamp = timestamp
        self.dateComplete = dateComplete
        self.documentID = documentID
    }
    
    // MARK: - Utilities
    
    // Parse data
    class func parseData(snapshot: QuerySnapshot?) -> [SavedShoppingList] {
        var savedShoppingLists = [SavedShoppingList]()
        
        guard let snapshot = snapshot else { return savedShoppingLists }
        
        for document in snapshot.documents {
            let data = document.data()
            
            let name = data[FirebaseCollection.SavedShoppingLists.name] as? String ?? Common.Value.empty
            let timestamp = data[FirebaseCollection.SavedShoppingLists.timestamp] as? Timestamp ?? Timestamp()
            let dateComplete = data[FirebaseCollection.SavedShoppingLists.dateCompleted] as? Timestamp ?? Timestamp()
            let numberOfItems = data[FirebaseCollection.SavedShoppingLists.numberOfItems] as? Int ?? 0
            let tags = data[FirebaseCollection.SavedShoppingLists.tags] as? String ?? Common.Value.empty
            let shared = data[FirebaseCollection.SavedShoppingLists.shared] as? Bool ?? false
            let completed = data[FirebaseCollection.SavedShoppingLists.completed] as? Bool ?? false
            let saved = data[FirebaseCollection.SavedShoppingLists.saved] as? Bool ?? false
            
            let documentID = document.documentID
            
            let savedShoppingList = SavedShoppingList(name: name, numberOfItems: numberOfItems, shared: shared, completed: completed, saved: saved, tags: tags, timestamp: timestamp, dateComplete: dateComplete, documentID: documentID)
            if savedShoppingList.saved {
                savedShoppingLists.append(savedShoppingList)
            }
        }
        
        return savedShoppingLists
    }
}
