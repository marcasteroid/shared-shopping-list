//
//  AppDelegate.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import TwitterKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?

    // Realm Queue
    let REALM_QUEUE = DispatchQueue(label: "realmQueue")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Firabase configuration
        FirebaseApp.configure()
        
        // Google authentication configuration
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance()?.delegate = self
        
        // Facebook authentication configuration
        
        // Twitter authentication configuration
        let consumerKey = Bundle.main.object(forInfoDictionaryKey: "consumerKey")
        let consumerSecret = Bundle.main.object(forInfoDictionaryKey: "consumerSecret")
        
        if let consumerKey = consumerKey as? String, let consumerSecret = consumerSecret as? String {
            TWTRTwitter.sharedInstance().start(withConsumerKey: consumerKey, consumerSecret: consumerSecret)
        }
        
        // Create Realm database for Country codes
        loadJSONData()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        // Google
        //let returnGoogle = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        let returnGoogle = GIDSignIn.sharedInstance().handle(url)
        
        // Twitter
        let returnTwitter = TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        
        return returnGoogle || returnTwitter
    }
    
    // MARK: - Google Sign In
    // Sign in
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            debugPrint("Could not login with google: \(error.localizedDescription)")
        } else {
            // Get authentication credentials from the user
            // Controller
            guard let controller = GIDSignIn.sharedInstance()?.delegate as? LoginViewController else { return }
            // Authentication
            guard let authentication = user.authentication else { return }
            // Credential
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
            controller.firebaseLogin(credential)
        }
    }
    
    // MARK: - Realm Database
    // Create Realm database for Country codes
    func loadJSONData() {
        guard let path = Bundle.main.path(forResource: "country", ofType: "json") else { return }
        
        REALM_QUEUE.sync {
            let url = URL(fileURLWithPath: path)
            
            do {
                let data = try Data(contentsOf: url)
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                guard let array = json as? [Any] else { return }
                for element in array {
                    guard let elementDict = element as? [String: Any] else { return }
                    guard let name = elementDict["name"] as? String, let code = elementDict["code"] as? String, let iso = elementDict["iso"] as? String else { return }
                    Country.addCountryToRealm(name: name, code: code, iso: iso)
                }
                
            } catch let error {
                debugPrint("Data error: \(error.localizedDescription)")
            }
        }
    }

}

