//
//  ForgotPasswordViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {

    // MARK: - IBOutlets
    // Email text field
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBActions
    
    // Reset button action
    @IBAction func resetButtonWasTapped(_ sender: Any) {
        // Check if the email text box is not empty
        guard let email = emailTextField.text, email.isNotEmpty else {
            // Handle email error
            handleUserAuthenticationError(forError: UserUI.Authentication.Error.emailTextFieldError)
            return
        }
        // Send password reset email
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            // Check if there is an error
            if let error = error {
                // Handle Firebase authentication error
                self.handleFirebaseAuthError(error: error)
                return
            }
            // Dismiss
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    // Cancel button action
    @IBAction func cancelButtonWasTapped(_ sender: Any) {
        // Dismiss
        dismiss(animated: true, completion: nil)
    }
    
}
