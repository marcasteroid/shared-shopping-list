//
//  TagCollectionViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 04/06/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {

    // MARK: - IBOutlets
    // Tag name label
    @IBOutlet weak var tagName: UILabel!
    // Tag name label max width constraint
    @IBOutlet weak var tagNameLabelMaxWidthContraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        // Set cell background color
        self.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1)
        // Set tag name label text color
        self.tagName.textColor = UIColor(named: "Flat Blue")
        // Set cell corner radius
        self.layer.cornerRadius = 15
        // Set cell border width
        self.layer.borderWidth = 1
        // Set cell border color
        self.layer.borderColor = UIColor(named: "Flat Blue")?.cgColor
        // Set tag name label max width constraint
        self.tagNameLabelMaxWidthContraint.constant = UIScreen.main.bounds.width - 8 * 2 - 8 * 2
    }

}
