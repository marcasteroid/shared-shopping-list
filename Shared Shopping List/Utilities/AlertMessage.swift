//
//  AlertMessage.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//


// Insert description

import Foundation
import UIKit

class AlertController: UIViewController {
    static let instance = AlertController()
    
    // Show an alert
    func showInfoAlert(alertControllerTitle: String, alertControllerMessage: String, alertActionTitle: String) {
        // Initialize alert controller
        let alertController = UIAlertController(title: alertControllerTitle, message: alertControllerMessage, preferredStyle: .alert)
        // Initialize action
        let action = UIAlertAction(title: alertActionTitle, style: .default, handler: nil)
        // Configure alert controller
        alertController.addAction(action)
        // Present alert controller
        present(alertController, animated: true, completion: nil)
    }
}
