//
//  UserUI.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 23/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

// User UI errors constants

struct UserUI {
    
    // Authentication errors
    struct Authentication {
        struct Error {
            // Email text field error
            static let emailTextFieldError: String = "emailTextFieldError"
            // Password text field error
            static let passwordTextFieldError: String = "passwordTextFieldError"
        }
    }
    
    // Create user errors
    struct Create {
        struct Error {
            // Username text field error
            static let usernameTextFieldError: String = "usernameTextFieldError"
            // Email text field error
            static let emailTextFieldError: String = "emailTextFieldError"
            // Country text field error
            static let countryTextFieldError: String = "countryTextFieldError"
            // Country not found
            static let countryNotFoundError: String = "countryNotFoundError"
            // Phone number text field error
            static let phoneNumberTextFieldError: String = "phoneNumberTextFieldError"
            // Phone number badly formatted
            static let phoneNumberBadlyFormattedError: String = "phoneNumberBadlyFormatted"
            // Password text field error
            static let passwordTextFieldError: String = "passwordTextFieldError"
        }
    }
    
    // Edit user profile errors
    struct Edit {
        struct Error {
            // First name text field error
            static let firstNameTextFieldError: String = "firstNameTextFieldError"
            // Last name text field error
            static let lastNameTextFieldError: String = "lastNameTextFieldError"
            // Country text field error
            static let countryTextFieldError: String = "countryTextFieldError"
            // Country not found
            static let countryNotFoundError: String = "countryNotFoundError"
            // Phone number text field error
            static let phoneNumberTextFieldError: String = "phoneNumberTextFieldError"
            // Phone number badly formatted
            static let phoneNumberBadlyFormattedError: String = "phoneNumberBadlyFormatted"
            // Age text field error
            static let ageTextFieldError: String = "ageTextFieldError"
            // Age text field contains characters error
            static let ageTextFieldContainsCharactersError: String = "ageTextFieldContainsCharactersError"
        }
    }
}


