//
//  LocalPath.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct LocalPath {
    // File
    struct File {
        // JSON file
        struct JSON {
            // Country JSON file
            static let name: String = "country"
            // Type
            static let type: String = "json"
        }
    }
}
