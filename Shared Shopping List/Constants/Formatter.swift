//
//  Formatter.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct Formatter {
    // Date formatter
    struct Date {
        static let dayMonthHMin: String = "d MMMM, HH:mm"
    }
    // Phone number formatter
    struct PhoneNumber {
        // Regex
        static let regex: String = "^[0-9]{10,14}$"
        // NSPredicate
        static let predicate: String = "SELF MATCHES %@"
    }
}
