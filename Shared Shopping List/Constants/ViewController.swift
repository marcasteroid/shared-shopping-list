//
//  ViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct ViewController {
    // Parent view controller
    static let parentViewController: String = "ParentViewController"
    // First view controller
    static let firstViewController: String = "FirstViewController"
    // Login
    static let loginViewController: String = "LoginViewController"
    // Create user
    static let createUserViewController: String = "CreateUserViewController"
    // Tab bar controller
    static let tabBarController: String = "TabBarController"
    // Shopping List
    static let shoppingListsViewController: String = "ShoppingListsViewController"
    // Create new shopping list
    static let createNewShoppingListViewController: String = "CreateNewShoppingListViewController"
    // User profile
    static let userProfileViewController: String = "UserProfileViewController"
    // Add item
    static let addItemViewController: String = "AddItemViewController"
    // Group list
    static let groupListViewController: String = "GroupListViewController"
    // Create new group
    static let createNewGroupViewController: String = "CreateNewGroupViewController"
    // Select group view controller
    static let selectGroupViewController: String = "SelectGroupViewController"
}
