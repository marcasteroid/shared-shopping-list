//
//  Common.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 23/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Common constants

import Foundation

struct Common {
    // String related constants
    struct Value {
        // Empty string
        static let empty: String = ""
        // Single space character
        static let singleSpace: String = " "
        // Country code separator
        static let countryCodeSeparator: String = "#"
    }
    
    // Search query
    struct Query {
        // Phone number
        static let phoneNumber: String = "phoneNumber"
    }
}
