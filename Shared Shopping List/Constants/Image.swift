//
//  Image.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 26/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct Image {
    
    // Image
    struct Image {
        struct Placeholder {
            // Avatar placeholder
            static let avatar: String = "avatar"
            // White box image
            static let whiteBox: String = "white-box.png"
        }
    }
}
