//
//  CollectionViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 10/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct CollectionViewCell {
    // Cell height
    static let height: Int = 80
    
    // Number of sections
    static let numberOfSections: Int = 1
    
    // User group cell
    struct UserGroupCell {
        // Identifier
        static let identifier: String = "userGroupCell"
    }
}

