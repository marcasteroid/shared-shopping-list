//
//  TableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Constants related to table view cell

import Foundation
import UIKit

struct TableViewCell {
    // Cell height
    static let height: CGFloat = 80
    
    // Number of sections
    static let numberOfSections: Int = 1

    // Shopping lists
    struct ShoppingLists {
        // Identifier
        static let identifier: String = "shoppingListCell"
    }
    
    // Saved shopping lists
    struct SavedShoppingLists {
        // Identifier
        static let identifier: String = "savedShoppingListCell"
    }
    
    // Item list
    struct ItemList {
        // Identifier
        static let identifier: String = "itemCell"
    }
    
    // Actions list
    struct ActionsList {
        // Identifier
        static let identifier: String = "actionCell"
    }
    
    // Address Book Contacts list cell
    struct AddressBookContacsListCell {
        // Identifier
        static let identifier: String = "addressBookContactCell"
    }
    
    // Users in group list
    struct UsersGroupListCell {
        static let identifier: String = "usersGroupListCell"
    }
    
    // Groups list
    struct GroupList {
        // Indentifier
        static let identifier: String = "groupCell"
    }
}

