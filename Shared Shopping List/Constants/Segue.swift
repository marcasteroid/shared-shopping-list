//
//  Segue.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Constants related to segues

import Foundation

struct Segue {
    // Destination
    struct Destination {
        // Login
        static let login: String = "toLogin"
        // To create account
        static let createAccount: String = "toCreateAccount"
    }
    // Unwind
    struct Unwind {
        // Unwind to login
        static let toLogin: String = "unwindToLogin"
    }
    // Storyboard
    struct Storyboard {
        // Main
        static let main: String = "Main"
        // Login view controller
        static let loginViewController: String = "LoginViewController"
        // Create user view controller
        static let createUserViewController: String = "CreateUserViewController"
        // User profile view controller
        static let userProfileViewController: String = "UserProfileViewController"
        // Add item view controller
        static let addItemViewController: String = "AddItemViewController"
        // New shopping list view controller
        static let newShoppingListViewController: String = "NewShoppingListViewController"
        // New shared shopping list view controller
        static let newSharedShoppingListViewController: String = "NewSharedShoppingListViewController"
        // Group title view controller
        static let groupTitleViewController: String = "GroupTitleViewController"
        // Group list
        static let groupListViewController: String = "GroupListViewController"
        // Edit user profile view controller
        static let editUserProfileViewController: String = "EditUserProfileViewController"
        // Select group view controller
        static let selectGroupViewController: String = "SelectGroupViewController"
        // Group detail view controller
        static let groupDetailViewController: String = "GroupDetailViewController"
        // New group view controller
        static let addNewGroupViewController: String = "CreateNewGroupViewController"
        // Saved personal shopping lists
        static let savedPersonalShoppingLists: String = "SavedPersonalShoppingLists"
    }
}
