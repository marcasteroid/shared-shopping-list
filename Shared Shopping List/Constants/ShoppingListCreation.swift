//
//  ShoppingListCreation.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/06/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Shopping list creation error

import Foundation

struct ShoppingListCreation {
    
    // Create new shopping list
    struct CreateNewShoppingList {
        struct Error {
            // Shopping list name text field error
            static let shoppingListNameTextFieldError: String = "shoppingListNameTextFieldError"
            // Shopping list tags text field error
            static let shoppingListTagsTextFieldError: String = "shoppingListTagsFieldError"
            // Shopping list item name text field error
            static let shoppingListItemNameTextFieldError: String = "shoppingListItemNameTextFieldError"
        }
    }
}
