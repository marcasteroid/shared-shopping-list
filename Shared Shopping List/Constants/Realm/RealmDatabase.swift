//
//  RealmDatabase.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct RealmDatabase {
    // Attributes
    struct Attributes {
        // Primary key
        static let primaryKey: String = "id"
    }
    
    // Realm queue
    struct Queue {
        // Dispatch queue
        static let dispatchQueue = DispatchQueue(label: "realmQueue")
    }
}
