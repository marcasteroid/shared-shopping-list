//
//  FirebaseDatabase.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Constants related to Firebase

import Foundation
import Firebase

struct FirebaseDatabase {
    // Database reference: sharedshoppinglist-9a4c2
    static let db_base = Database.database().reference()
    // Database reference
    static let db = Firestore.firestore()
}
