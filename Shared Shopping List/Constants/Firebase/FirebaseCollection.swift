//
//  FirebaseCollection.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Constants related to Firebase collections

import Foundation

struct FirebaseCollection {
    
    // Users collection
    struct Users {
        // Collection name
        static let collectionName: String = "users"
        // Document reference
        static let usersReference: String = "users"
        // Username
        static let username: String = "username"
        // Email address
        static let emailAddress: String = "email"
        // Phone number
        static let phoneNumber: String = "phoneNumber"
        // Phone number with prefix
        static let phoneNumberWithPrefix: String = "phoneNumberWithPrefix"
        // Image URL
        static let imageURL: String = "imageURL"
        // ID
        static let id: String = "id"
        // Date created
        static let dateCreated: String = "dateCreated"
        // Groups
        static let groups: String = "groups"
        // First name
        static let firstName: String = "firstName"
        // Last name
        static let lastName: String = "lastName"
        // Age
        static let age: String = "age"
    }
    
    // Shopping lists collection
    struct ShoppingLists {
        // Collection name
        static let collectionName: String = "personalShoppingLists"
        // Name
        static let name: String = "name"
        // Tags
        static let tags: String = "tags"
        // Time created
        static let timestamp: String = "timestamp"
        // Date completed
        static let dateCompleted: String = "dateCompleted"
        // Number of items
        static let numberOfItems: String = "numberOfItems"
        // ID
        static let id: String = "id"
        // Shared
        static let shared: String = "shared"
        // Completed
        static let completed: String = "completed"
        // Saved
        static let saved: String = "saved"
        // Admin ID
        static let adminID: String = "adminID"
    }
    
    // Shared shopping lists collection
    struct SharedShoppingLists {
        // Collection name
        static let collectionName: String = "sharedShoppingLists"
        // Name
        static let name: String = "name"
        // Tags
        static let tags: String = "tags"
        // Time created
        static let timestamp: String = "timestamp"
        // Date completed
        static let dateCompleted: String = "dateCompleted"
        // Number of items
        static let numberOfItems: String = "numberOfItems"
        // ID
        static let id: String = "id"
        // Shared
        static let shared: String = "shared"
        // Completed
        static let completed: String = "completed"
        // Saved
        static let saved: String = "saved"
        // Admin ID
        static let adminID: String = "adminID"
    }
    
    // Saved shopping lists collection
    struct SavedShoppingLists {
        // Collection name
        static let collectionName: String = "shopping-lists"
        // Name
        static let name: String = "name"
        // Tags
        static let tags: String = "tags"
        // Time created
        static let timestamp: String = "timestamp"
        // Date completed
        static let dateCompleted: String = "dateCompleted"
        // Number of items
        static let numberOfItems: String = "numberOfItems"
        // ID
        static let id: String = "id"
        // Shared
        static let shared: String = "shared"
        // Completed
        static let completed: String = "complete"
        // Saved
        static let saved: String = "saved"
        // Admin ID
        static let adminID: String = "adminID"
    }
    
    // Groups
    struct Groups {
        // Admin identifier
        static let adminID: String = "adminID"
        // Name
        static let name: String = "name"
        // Member count
        static let membersCount: String = "membersCount"
        // Members
        static let members: String = "members"
        // Time created
        static let timestamp: String = "timestamp"
        // Shopping lists ID
        static let shoppingListsID: String = "shoppingListsID"
        // Collection name
        static let collectionName: String = "groups"
        // Document ID
        static let documentID: String = "documentID"
        // Label
        static let label: String = "label"
    }
    
    // Shopping list items
    struct Items {
        // Collection name
        static let collectionName: String = "items"
        // Item name
        static let name: String = "name"
        // Done
        static let done: String = "done"
        // Shopping list ID
        static let shoppingListID: String = "shoppingListID"
        // Time created
        static let timestamp: String = "timestamp"
    }
}
