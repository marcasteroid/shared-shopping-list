//
//  Services.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 24/05/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct Services {
    // Review service
    struct ReviewService {
        // Last request userdefault key identifier
        static let key: String = "ReviewLastRequest"
        // Elapsed time since last review
        static let ElapsedTime: Int = -7
    }
}
