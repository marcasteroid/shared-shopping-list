//
//  UserProfileActions.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 04/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct UserProfileActions {
    // Actions
    struct Action {
        // Recommend this app to a friend
        static let recommend: String = "Recommend this app to a friend"
        // Send us your feedback
        static let sendFeedback: String = "Send us your feedback"
        // Review
        static let review: String = "Review this app"
        // Notifications
        static let notifications: String = "Notifications"
        // Credits
        static let credits: String = "Credits"
        // Settings
        static let settings: String = "Settings"
        // Help
        static let help: String = "Help"
        // About
        static let about: String = "About"
        // Edit profile
        static let edit: String = "Edit profile"
        // Delete account
        static let deleteAccount: String = "Delete account"
        // Logout
        static let logout: String = "Logout"
    }
    
    // Action image
    struct Image {
        // Recommend this app to a friend
        static let recommend: String = "heart.png"
        // Send us your feedback
        static let sendFeedback: String = "thumbs-up.png"
        // Review
        static let review: String = "feather.png"
        // Notifications
        static let notifications: String = "bell.png"
        // Credits
        static let credits: String = "star.png"
        // Settings
        static let settings: String = "settings.png"
        // Help
        static let help: String = "life-buoy.png"
        // About
        static let about: String = "info.png"
        // Edit profile
        static let edit: String = "user-check.png"
        // Delete account
        static let deleteAccount: String = "x.png"
        // Logout
        static let logout: String = "log-out.png"
    }
}
