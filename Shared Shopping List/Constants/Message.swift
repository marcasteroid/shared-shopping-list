//
//  Message.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 27/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

// Constants related to messages to the user

import Foundation

struct Message {
    
    // Shopping lists messages
    struct ShoppingLists {
        // No shopping lists
        static let noShoppingLists: String = "You have no shopping lists..."
        // More items
        static let moreItems: String = "items"
        // Single item
        static let singleItem: String = "item"
    }
    
    // Group messages
    struct Groups {
        // Members
        static let members: String = "members"
    }
    
    // Create user messages
    struct CreateUser {
        // User submit information errors
        struct Error {
            // Username
            static let username: String = "username"
            // Email
            static let email: String = "email"
            // Country
            static let country: String = "country"
            // Phone number
            static let phoneNumber: String = "phoneNumber"
            // Password
            static let password: String = "password"
        }
        
        struct Alert {
            // Actions
            struct Actions {
                // OK
                static let ok: String = "OK"
            }
            // Error messages
            struct Error {
                // Title
                static let title: String = "Create user error"
            }
            // Username
            struct Username {
                // Title
                static let title: String = "Username missing"
                // Message to the user
                static let message: String = "You must provide a username"
            }
            // Email
            struct Email {
                // Title
                static let title: String = "Email missing"
                // Message to the user
                static let message: String = "You must provide a valid email address"
            }
            // Country
            struct Country {
                // Title
                static let title: String = "Country missing"
                // Message to the user
                static let message: String = "You must provide a valid country name"
            }
            // Country not found
            struct CountryNotFound {
                // Title
                static let title: String = "Country not found"
                // Message
                static let message: String = "You entered an invalid country name. Please check !"
            }
            // Phone number
            struct PhoneNumber {
                // Title
                static let title: String = "Phone number missing"
                // Message to the user
                static let message: String = "You must provide a valid phone number"
            }
            // Phone number badly formatted
            struct PhoneNumberBadlyFormatted {
                // Title
                static let title: String = "Invalid phone number"
                // Message to the user
                static let message: String = "You entered an invalid phone number. Please check !"
            }
            // Password
            struct Password {
                // Title
                static let title: String = "Password missing"
                // Message to the user
                static let message: String = "You must provide a password"
            }
            // Unknown
            struct Unknown {
                // Title
                static let title: String = "Unknown error"
                // Message to the user
                static let message: String = "Unknown error occurred. Please try again later !"
            }
        }
    }
    
    // Edit user messages
    struct EditUser {
        // User submit information errors
        struct Error {
            // First name
            static let firstName: String = "firstName"
            // Last name
            static let lastName: String = "lastName"
            // Country
            static let country: String = "country"
            // Phone number
            static let phoneNumber: String = "phoneNumber"
            // Age
            static let age: String = "age"
        }
        
        struct Alert {
            // Actions
            struct Actions {
                // OK
                static let ok: String = "OK"
            }
            // Error messages
            struct Error {
                // Title
                static let title: String = "Edit profile error"
            }
            // First name
            struct FirstName {
                // Title
                static let title: String = "First name missing"
                // Message to the user
                static let message: String = "You must provide your first name"
            }
            // Last name
            struct LastName {
                // Title
                static let title: String = "Last name missing"
                // Message to the user
                static let message: String = "You must provide your last name"
            }
            // Country
            struct Country {
                // Title
                static let title: String = "Country missing"
                // Message to the user
                static let message: String = "You must provide a valid country name"
            }
            // Country not found
            struct CountryNotFound {
                // Title
                static let title: String = "Country not found"
                // Message
                static let message: String = "You entered an invalid country name. Please check !"
            }
            // Phone number
            struct PhoneNumber {
                // Title
                static let title: String = "Phone number missing"
                // Message to the user
                static let message: String = "You must provide a valid phone number"
            }
            // Phone number badly formatted
            struct PhoneNumberBadlyFormatted {
                // Title
                static let title: String = "Invalid phone number"
                // Message to the user
                static let message: String = "You entered an invalid phone number. Please check !"
            }
            // Age
            struct AgeMissing {
                // Title
                static let title: String = "Age missing"
                // Message to the user
                static let message: String = "You must provide your age"
            }
            // Age user input non decimal digits
            struct AgeContainsCharacters {
                // Title
                static let title: String = "Age must be a number"
                // Message to the user
                static let message: String = "You must provide your age in numbers"
            }
            
            // Unknown
            struct Unknown {
                // Title
                static let title: String = "Unknown error"
                // Message to the user
                static let message: String = "Unknown error occurred. Please try again later !"
            }
        }
    }
    
    // Shopping list creation messages
    struct ShoppingListCreation {
        // Shopping list creation errors
        struct Error {
            // Shopping list name
            static let name: String = "name"
            // Shopping list tag
            static let tag: String = "tag"
        }
        // Alert
        struct Alert {
            // Actions
            struct Actions {
                // OK
                static let ok: String = "OK"
            }
            // Error messages
            struct Error {
                // Title
                static let title: String = "Error creating new shopping list"
            }
            // Shopping list name
            struct Name {
                // Title
                static let title: String = "Shopping list name missing"
                // Message
                static let message: String = "You must provide a valid name for the shopping list"
            }
            // Shopping list tags
            struct Tags {
                // Title
                static let title: String = "Shopping list tag missing"
                // Message
                static let message: String = "You must provide a valid tag"
            }
            // Shopping list item name
            struct ItemName {
                // Title
                static let title: String = "Item name missing"
                // Message
                static let message: String = "You must provide a valid item name"
            }
            // Unknown
            struct Unknown {
                // Title
                static let title: String = "Unknown error"
                // Message to the user
                static let message: String = "Unknown error occurred. Please try again later !"
            }
        }
    }
    
    // Group creation messages
    struct GroupCreation {
        // Group creation error messages
        struct Error {
            // Group title
            static let title: String = "title"
        }
        
        // Alert
        struct Alert {
            struct Actions {
                // OK
                static let ok: String = "OK"
            }
            
            struct Error {
                static let title: String = "Error creating group"
            }
            
            struct Title {
                // Title
                static let title: String = "Group title missing"
                // Message
                static let message: String = "You must provide a valid title for the group"
            }
            
            // Unknown
            struct Unknown {
                // Title
                static let title: String = "Unknown error"
                // Message to the user
                static let message: String = "Unknown error occurred. Please try again later !"
            }
        }
    }
    
    // Standard login messages
    struct StandardLogin {
        // User submit information errors
        struct Error {
            // Email
            static let email: String = "email"
            // Password
            static let password: String = "password"
        }
        
        // Alert
        struct Alert {
            // Actions
            struct Actions {
                // OK
                static let ok: String = "OK"
            }
            // Error messages
            struct Error {
                // Title
                static let title: String = "Error signing in"
            }
            // Email
            struct Email {
                // Title
                static let title: String = "Email missing"
                // Message to the user
                static let message: String = "You must provide a valid email address"
            }
            // Password
            struct Password {
                // Title
                static let title: String = "Password missing"
                // Message to the user
                static let message: String = "You must provide a password"
            }
            // Unknown
            struct Unknown {
                // Title
                static let title: String = "Unknown error"
                // Message to the user
                static let message: String = "Unknown error occurred. Please try again later !"
            }
            // Notifications
            struct Notification {
                // User data did change
                static let userDataDidChange: String = "notificationUserDataChanged"
            }
        }
    }
}
