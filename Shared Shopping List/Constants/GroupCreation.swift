//
//  GroupCreation.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

struct GroupCreation {
    
    // Create group
    struct CreateNewGroup {
        struct Error {
            // Group name text field error
            static let groupNameTextFieldError: String = "groupNameTextFieldError"
        }
    }
}
