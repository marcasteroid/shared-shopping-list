//
//  SavedShoppingListsViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 09/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SavedShoppingListsViewController: UIViewController {

    // MARK: - IBOutlets
    
    // Shopping lists table view
    @IBOutlet weak var savedShoppingListsTableView: UITableView!
    // No shopping lists info message
    @IBOutlet weak var noSavedShoppingListsInfoMessage: UIStackView!
    
    // MARK: - Variables
    
    // Saved shopping list service
    private var savedShoppingListService = SavedShoppingListsService()
    // Saved shopping lists document reference
    private var savedShoppingListReference: DocumentReference!
    // Saved shopping lists collection reference
    private var savedShoppingListsCollectionReference: CollectionReference!
    // Saved shopping list listner
    private var savedShoppingListsListener: ListenerRegistration!
    // Array of shopping lists
    private var savedShoppingLists = [SavedShoppingList]()
    // Authentication handler
    private var handle: AuthStateDidChangeListenerHandle?
    
    // MARK: - Main
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Table view delegate
        savedShoppingListsTableView.delegate = self
        // Table view data source
        savedShoppingListsTableView.dataSource = self
        // Estimated row height
        savedShoppingListsTableView.estimatedRowHeight = 80
        // Row height automatic dimension
        savedShoppingListsTableView.rowHeight = UITableView.automaticDimension
        
        // Saved shopping list reference
        savedShoppingListsCollectionReference = Firestore.firestore().collection(FirebaseCollection.ShoppingLists.collectionName)
        
        // Set color for swipe aciont icons
        UIImageView.appearance(whenContainedInInstancesOf: [UITableView.self]).tintColor = UIColor(red: 0.0, green: 0.53, blue: 1.0, alpha: 1.0)
        // Set tab bar item text
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .selected)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            // Check if the user is logged in
            if user == nil {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.setListener()
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Remove saved shopping list listener
        if savedShoppingListsListener != nil {
            savedShoppingListsListener.remove()
        }
        savedShoppingLists.removeAll()
        savedShoppingListsTableView.reloadData()
    }
    
    // MARK: - Utilities
    
    private func setListener() {
        // Snapshot listener for shopping lists collection and order by date of creation in descending order (most recent)
        guard let userID = Auth.auth().currentUser?.uid else { return }
        savedShoppingListsListener = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName)
            .order(by: FirebaseCollection.ShoppingLists.timestamp, descending: true)
            .addSnapshotListener { (snapshot, error) in
                // Check for errors
                if let error = error {
                    debugPrint("Error fetching documents \(error.localizedDescription)")
                } else {
                    self.savedShoppingLists.removeAll()
                    self.savedShoppingLists = SavedShoppingList.parseData(snapshot: snapshot)
                    if self.savedShoppingLists.count == 0 {
                        self.noSavedShoppingListsInfoMessage.isHidden = false
                        self.savedShoppingListsTableView.reloadData()
                    } else {
                        self.savedShoppingListsTableView.reloadData()
                        self.noSavedShoppingListsInfoMessage.isHidden = true
                    }
                }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Back button action
    
    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension SavedShoppingListsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedShoppingLists.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewCell.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCell.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.SavedShoppingLists.identifier) as? SavedShoppingListTableViewCell {
            cell.configureCell(savedShoppingList: savedShoppingLists[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    // MARK: - Table view swipe actions
    
    // Leading Swipe action
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let width: CGFloat = 25
        let height: CGFloat = 25
        
        // Done action
        let buyAgain = UIContextualAction(style: .normal, title: nil) { (action, view, nil) in
            // Get user id
            guard let userID = Auth.auth().currentUser?.uid else { return }
            self.savedShoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.savedShoppingLists[indexPath.row].documentID)
            
            self.savedShoppingListService.buyAgain(savedShoppingList: self.savedShoppingLists[indexPath.row], shoppingListReference: self.savedShoppingListReference)
        }
        
        let buyAgainImage = UIGraphicsImageRenderer(size: CGSize(width: width, height: height)).image { (context) in
            context.stroke(CGRect(x: 0, y: 0, width: width, height: height), blendMode: .clear)
            ItemActionIcon.drawBuyAgainActionCanvas(frame: CGRect(x: 0, y: 0, width: width, height: height), resizing: .aspectFit)
        }
        buyAgain.image = buyAgainImage
        buyAgain.backgroundColor = .white
        
        let config = UISwipeActionsConfiguration(actions: [buyAgain])
        config.performsFirstActionWithFullSwipe = false
        self.savedShoppingListsTableView.isEditing = false
        return config
    }
    
    // Trailing swipe action
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let width: CGFloat = 23
        let height: CGFloat = 23
        
        // Delete action
        let delete = UIContextualAction(style: .normal, title: nil) { (action, view, nil) in
            print("Item at index \(indexPath.row) is going to be deleted !")
        }
        
        let deleteImage = UIGraphicsImageRenderer(size: CGSize(width: width, height: height)).image { (context) in
            context.stroke(CGRect(x: 0, y: 0, width: width, height: height), blendMode: .clear)
            ItemActionIcon.drawDeleteActionCanvas(frame: CGRect(x: 0, y: 0, width: width, height: height), resizing: .aspectFit)
        }
        
        delete.image = deleteImage
        delete.backgroundColor = .white
        
        let config = UISwipeActionsConfiguration(actions: [delete])
        config.performsFirstActionWithFullSwipe = false
        self.savedShoppingListsTableView.isEditing = false
        return config
    }

}
