//
//  GroupDetailViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class GroupDetailViewController: UIViewController {

    // MARK: - IBOutlets
    // Group name label
    @IBOutlet weak var groupNameLabel: UILabel!
    // Number of members label
    @IBOutlet weak var numberOfMembersLabel: UILabel!
    // Number of shopping lists
    @IBOutlet weak var numberOfShoppingListsLabel: UILabel!
    // Shopping lists table view
    @IBOutlet weak var shoppingListsTableView: UITableView!
    // Users group collection view
    @IBOutlet weak var usersGroupCollectionView: UICollectionView!
    
    // MARK: - Properties
    // Group
    var group: Group!
    // Users in group
    var usersGroup = [UserGroup]()
    
    // MARK: - Main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - Setup view
        setupView()
        
        // MARK: - Shopping lists table view settings
        // Shopping lists table view delegate
        //shoppingListsTableView.delegate = self
        // Shopping lists table view datasource
        //shoppingListsTableView.dataSource = self
        // Estimated row height
        shoppingListsTableView.estimatedRowHeight = TableViewCell.height
        // Row height: automatic dimension
        shoppingListsTableView.rowHeight = UITableView.automaticDimension
        
        // MARK: - Users in group collection view
        // Users group collection view delegate
        //usersGroupCollectionView.delegate = self
        // Users groups collection view datasource
        //usersGroupCollectionView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    func setupView() {
        groupNameLabel.text = group.label.capitalizeFirstLetter()
        numberOfMembersLabel.text = "\(group.membersCount!) members"
        numberOfShoppingListsLabel.text = "\(String(group.shoppingListsID.count)) shopping lists"
    }
        
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBActions
    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
/*
extension GroupDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.UserGroupCell.identifier, for: indexPath) as? UserGroupCollectionViewCell else { return UserGroupCollectionViewCell() }
        let userGroup = usersGroup[indexPath.row]
        cell.updateView(userGroup: userGroup)
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return CollectionViewCell.numberOfSections
    }
    
}


extension GroupDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    }
}
*/
