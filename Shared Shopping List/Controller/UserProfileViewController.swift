//
//  SideMenuViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 24/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
import TwitterKit
import Kingfisher

class UserProfileViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    // Table view
    @IBOutlet weak var tableView: UITableView!
    // Username label
    @IBOutlet weak var usernameLabel: UILabel!
    // User email address label
    @IBOutlet weak var userEmailAddressLabel: UILabel!
    // User profile image
    @IBOutlet weak var userProfileImage: CustomImage!
    // First letter username
    @IBOutlet weak var firstLetterUsernameLabel: UILabel!
    
    
    // MARK: - Variables
    // Array of user profile actions
    private(set) public var userProfileActionsList = [UserProfileAction]()
    // Request review instance
    let reviewService = ReviewService.instance
    // Listener
    var listener: ListenerRegistration!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userProfileActionsList = UserProfileActionsService.instance.getActions()
        
        // Set tableview
        tableView.delegate = self
        tableView.dataSource = self
        
        // Set tab bar item text
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .selected)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        userDataDidChange()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        listener.remove()
    }
    
    func initActions(userProfileAction: UserProfileAction) {
        userProfileActionsList = UserProfileActionsService.instance.getActions()
    }
    
    func userDataDidChange() {
        // Check if the user is signed in
        if Auth.auth().currentUser != nil { // User is signed in
            setupUIForSignedInUser()
        } else { // No user is signed in
            setupUIForSignedOutUser()
        }
    }
    
    // Sign out
    func signOut() {
        do {
            // Permorm sign out from the social platform
            signOutSocial()
            try Auth.auth().signOut()
            //self.dismiss(animated: true, completion: nil)
            let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
            let parentViewController = storyboard.instantiateViewController(withIdentifier: ViewController.parentViewController)
            self.present(parentViewController, animated: false, completion: nil)
        } catch let error {
            print("Error signing out: \(error)")
        }
    }
    
    // Sign out social
    func signOutSocial() {
        // Get current user
        guard let user = Auth.auth().currentUser else { return }
        // Get informations about user provider data
        for info in user.providerData {
            switch info.providerID {
            // Sign out from Google
            case GoogleAuthProviderID:
                GIDSignIn.sharedInstance()?.signOut()
            // Sign out from Facebook
            case FacebookAuthProviderID:
                print("Sign out from Facebook")
            // Sign out from Twitter
            case TwitterAuthProviderID:
                print("Sign out from Twitter")
            // Default
            default:
                break
            }
        }
    }
    
    // Delete a user account
    func deleteAccount() {
        // Get current user
        guard let user = Auth.auth().currentUser else { return }
        user.delete { (error) in
            // Check for errors
            if let error = error {
                fatalError("Cannot delete the user: \(error.localizedDescription)")
            } else {
                // Account deleted
                self.signOut()
            }
        }
    }
    
    // MARK: - Setup View
    
    // Set up the user interface when the user is logged in
    func setupUIForSignedInUser() {
        usernameLabel.text = Auth.auth().currentUser?.displayName?.capitalized
        userEmailAddressLabel.text = Auth.auth().currentUser?.email
        getUserProfileImage()
    }
    
    // Set up the user interface when the user is not logged in
    func setupUIForSignedOutUser() {
        // Set Login/logout button title to "login"
        usernameLabel.clear()
        userEmailAddressLabel.clear()
    }
    
    func getUserProfileImage() {
        guard let userID = Auth.auth().currentUser?.uid else { return }
        let documentReference = Firestore.firestore().collection(FirebaseCollection.Users.usersReference).document(userID)
        
        listener = documentReference.addSnapshotListener { (documentSnapshot, error) in
            if let error = error {
                print("Add snapshot listener error: \(error.localizedDescription)")
            }
            guard let data = documentSnapshot?.data() else { return }
            let imageURL = data[FirebaseCollection.Users.imageURL] as? String ?? Common.Value.empty
            if let url = URL(string: imageURL) {
                self.firstLetterUsernameLabel.isHidden = true
                let options: KingfisherOptionsInfo = [KingfisherOptionsInfoItem.transition(.fade(0.1)), .scaleFactor(UIScreen.main.scale)]
                self.userProfileImage.kf.indicatorType = .activity
                self.userProfileImage.kf.setImage(with: url, options: options)
            } else {
                guard let firstLetter = Auth.auth().currentUser?.displayName?.capitalized.first else {
                    self.firstLetterUsernameLabel.text = Common.Value.empty
                    return
                }
                self.firstLetterUsernameLabel.text = String(firstLetter)
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - IBActions
}

// Extensions
extension UserProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    // Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return TableViewCell.numberOfSections
    }
    
    // Number of rows in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return userProfileActionsList.count
    }
    
    // Height for row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCell.height)
    }
    
    // Cell creation
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.ActionsList.identifier, for: indexPath) as? UserProfileTableViewCell {
            cell.configureCell(userProfileActionsList: userProfileActionsList[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    // Row selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0: // Recommend this app to a friend
            print("Recommend this app to a friend")
        case 1: // Send us your feedback
            print("Send us your feedback")
        case 2: // Review this app
            requestReview()
        case 3: // Notifications
            print("Notifications")
        case 4: // Credits
            print("Credits")
        case 5: // Settings
            print("Settings")
        case 6: // Help
            print("Help")
        case 7: // About
            print("About")
        case 8:
            editUserProfile()
        case 9: // Delete account
            deleteAccount()
        case 10: // Sign out
            signOut()
        default:
            print("No actions")
        }
    }
    
    func editUserProfile() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: Segue.Storyboard.editUserProfileViewController, sender: nil)
        }
    }
    
    func requestReview() {
        DispatchQueue.main.async {
            self.reviewService.requestReview()
        }
    }

    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}
