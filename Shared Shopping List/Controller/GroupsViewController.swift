//
//  GroupsViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/07/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class GroupsViewController: UIViewController {
    
    // MARK: - IBOutlets
    // Groups table view
    @IBOutlet weak var groupsTableView: UITableView!
    // No groups info message
    @IBOutlet weak var noGroupsInfoMessage: UIStackView!
    
    // MARK: - Variables
    
    // Authentication handler
    private var handle:  AuthStateDidChangeListenerHandle?
    
    // Groups listner
    private var groupsListener: ListenerRegistration!
    // Array of groups
    private var groups = [Group]()
    // Group
    private var group: Group!
    // Users in group
    var usersGroup = [UserGroup]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Groups table view delegate
        groupsTableView.delegate = self
        // Groups table view datasource
        groupsTableView.dataSource = self
        
        // Estimated row height
        groupsTableView.estimatedRowHeight = TableViewCell.height
        // Row height: automatic dimension
        groupsTableView.rowHeight = UITableView.automaticDimension
        
        // Set tab bar item text
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .selected)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            // Check if the user is logged in
            if user == nil { // User not logged in
                self.dismiss(animated: true, completion: nil)
            } else { // User logged in
                self.getGroupsId { (returnedGroupIdArray) in
                    for groupID in returnedGroupIdArray {
                        self.setListener(groupID: groupID)
                    }
                }
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        // Remove group listner
        if groupsListener != nil {
            groupsListener.remove()
        }
        // Flush groups list array
        groups.removeAll()
        // Reload tableview
        groupsTableView.reloadData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    

    
    // MARK: - Utilities
    
    // Get groups
    fileprivate func getGroups() {
        guard let userID = Auth.auth().currentUser?.uid else { return }
        groupsListener = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.Groups.collectionName)
            .addSnapshotListener({ (snapshot, error) in
                // Check for errors
                if let error = error {
                    debugPrint("Error fetching documents: \(error.localizedDescription)")
                } else {
                    // Flush groups array
                    self.groups.removeAll()
                    // Parse data from Firebase
                    self.groups = Group.parseData(snapshot: snapshot)
                    
                    // Check if user has no groups
                    if self.groups.count == 0 {
                        // Show no groups info message
                        self.noGroupsInfoMessage.isHidden = false
                        // Reload table view
                        self.groupsTableView.reloadData()
                    } else { // The user has groups
                        // Reload tableview
                        self.groupsTableView.reloadData()
                        // Hide no groups info message
                        self.noGroupsInfoMessage.isHidden = true
                    }
                }
            })
    }
    
    // Set listener
    func setListener(groupID: String) {
        // Snapshot listner for groups collection and order by date of creation in descending order (most recent)
        
        groupsListener = Firestore.firestore().collection(FirebaseCollection.Groups.collectionName).document(groupID).collection(groupID).addSnapshotListener { (documentSnapshot, error) in
            if let error = error {
                debugPrint("Error fetching documents: \(error.localizedDescription)")
            } else {
                
                
                documentSnapshot?.documentChanges.forEach({ (change) in
                    let data = change.document.data()
                    let group = Group.init(data: data)
                    
                    switch change.type {
                    case .added:
                        self.onDocumentAdded(change: change, group: group)
                    case .modified:
                        self.onDocumentModified(change: change, group: group)
                    case .removed:
                        self.onDocumentRemoved(change: change)
                    @unknown default:
                        fatalError()
                    }
                })
                
                if self.groups.count == 0 {
                    self.noGroupsInfoMessage.isHidden = false
                    self.groupsTableView.reloadData()
                } else {
                    self.groupsTableView.reloadData()
                    self.noGroupsInfoMessage.isHidden = true
                }
            }
        }
    }
    
    func getGroupsId(handler: @escaping (_ groupsIdArray: [String]) -> ()) {
        var groupsIdArray = [String]()
        guard let userID = Auth.auth().currentUser?.uid else { return }
        // Get personal groups
        Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.Groups.collectionName).getDocuments { (querySnapshot, error) in
            if let error = error {
                debugPrint("Error getting documents: \(error)")
            } else {
                for document in querySnapshot!.documents {
                    let groupID = document.documentID
                    groupsIdArray.append(groupID)
                }
            }
            handler(groupsIdArray)
        }
    }
    
    @IBAction func addNewGroupListButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Storyboard.addNewGroupViewController, sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.Storyboard.groupDetailViewController {
            if let destinationViewController = segue.destination as? GroupDetailViewController {
                if let group = sender as? Group {
                    destinationViewController.group = group
                    destinationViewController.usersGroup = usersGroup
                }
            }
        }
    }
}

extension GroupsViewController: UITableViewDelegate, UITableViewDataSource {
    
    // Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewCell.numberOfSections
    }
    
    // Number of rows in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    // Height for row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCell.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.GroupList.identifier, for: indexPath) as? GroupTableViewCell {
            cell.configureCell(group: groups[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Segue.Storyboard.groupDetailViewController, sender: groups[indexPath.row])
    }
    
    func onDocumentAdded(change: DocumentChange, group: Group) {
        let newIndex = Int(change.newIndex)
        groups.insert(group, at: newIndex)
        groupsTableView.insertRows(at: [IndexPath(row: newIndex, section: 0)], with: .fade)
    }
    
    func onDocumentModified(change: DocumentChange, group: Group) {
        // Group list changed but remaind in the same position
        if change.newIndex == change.oldIndex {
            let index = Int(change.newIndex)
            groups[index] = group
            groupsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        } else {
            // Item changed and changed position
            let oldIndex = Int(change.oldIndex)
            let newIndex = Int(change.newIndex)
            groups.remove(at: oldIndex)
            groups.insert(group, at: newIndex)
            groupsTableView.moveRow(at: IndexPath(row: oldIndex, section: 0), to: IndexPath(row: newIndex, section: 0))
        }
    }
    
    func onDocumentRemoved(change: DocumentChange) {
        let oldIndex = Int(change.oldIndex)
        groups.remove(at: oldIndex)
        groupsTableView.deleteRows(at: [IndexPath(row: oldIndex, section: 0)], with: .fade)
    }
}
