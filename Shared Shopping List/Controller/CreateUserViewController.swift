//
//  CreateUserViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class CreateUserViewController: UIViewController {

    // MARK: - Properties
    
    // Create an instance of "CreateUser"
    var createUser = CreateUser()
    
    // MARK: - IBOutlets
    
    // Create user button
    @IBOutlet weak var createUserButton: UIButton!
    // Cancel create user button
    @IBOutlet weak var cancelUserButton: UIButton!
    // Username text field
    @IBOutlet weak var usernameTextField: UITextField!
    // Email text field
    @IBOutlet weak var emailTextField: UITextField!
    // Country text field
    @IBOutlet weak var countryTextField: UITextField!
    // Phone number
    @IBOutlet weak var phoneNumberTextField: UITextField!
    // Password text field
    @IBOutlet weak var passwordTextField: UITextField!
    // Header view
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the UI view
        setupView()
                
        // Country text field delegate
        countryTextField.delegate = self
    }
        
    // MARK: - Setup UI
    
    func setupView() {
        // Hide keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.handleTap))
        view.addGestureRecognizer(tap)
        
        // Listen for keyboard events
        // Keyboard will show
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        // Keyboard will hide
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Keyboard will change frame
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        // Disable create user button
        createUserButton.isEnabled = false
    }
    
    // Remove notifications
    deinit {
        // Stop listening for keyboard hide/show events
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    // Keyboard
    @objc func keyboardWillChange(notification: Notification) {
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        // Get bar height
        //let barHeight = self.navigationController?.navigationBar.frame.height ?? 0
        // Get status bar height
        //let statusBarHeight = UIApplication.shared.isStatusBarHidden ? CGFloat(0) : UIApplication.shared.statusBarFrame.height
        // Calculate distance from the top border
        //let topDistance = barHeight + statusBarHeight
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            // Hide navigation controller
            //self.navigationController?.navigationBar.isHidden = true
            // Hide header view
            self.headerView.isHidden = true
            //view.frame.origin.y = -(keyboardRect.height - topDistance)
            view.frame.origin.y = -keyboardRect.height
        } else {
            // Set navigation controller visible
            //self.navigationController?.navigationBar.isHidden = false
            // Set header view visible
            self.headerView.isHidden = false
            //view.frame.origin.y = topDistance
            view.frame.origin.y = 0
        }
    }
    
    // MARK: - IBActions

    // Create user button
    @IBAction func createrUserButtonWasTapped(_ sender: Any) {
        // Check if the username text field is not empty
        guard let username = usernameTextField.text, username.isNotEmpty else {
            // Handle username text field error
            handleUserCreationError(forError: UserUI.Create.Error.usernameTextFieldError)
            return
        }
        
        // Check if the email field is empty
        guard let email = emailTextField.text, email.isNotEmpty else {
            // Handle email text field error
            handleUserCreationError(forError: UserUI.Create.Error.emailTextFieldError)
            return
        }
        
        // Check if the country text field is empty
        guard let country = countryTextField.text, country.isNotEmpty else {
            // Handle country text field error
            handleUserCreationError(forError: UserUI.Create.Error.countryTextFieldError)
            return
        }
        
        // Check if the phonenumber field is empty
        guard let phoneNumber = phoneNumberTextField.text, phoneNumber.isNotEmpty else {
            handleUserCreationError(forError: UserUI.Create.Error.phoneNumberTextFieldError)
            return
        }
        
        // Check if the phone number is badly formatted
        guard let invalidPhoneNumber = phoneNumberTextField.text, invalidPhoneNumber.isNotValid else {
            // Handle phone number format error
            handleUserCreationError(forError: UserUI.Create.Error.phoneNumberBadlyFormattedError)
            return
        }

        // Check if the password field is empty
        guard let password = passwordTextField.text, password.isNotEmpty else {
            // Handle password text field error
            handleUserCreationError(forError: UserUI.Create.Error.passwordTextFieldError)
            return
        }
        
        // Create complete phone number: country code + phone number
        let phoneNumberWithPrefix = country + Common.Value.countryCodeSeparator + phoneNumber
        
        // Create user
        let user = User(username: username, email: email, password: password, phoneNumber: phoneNumber, phoneNumberWithPrefix: phoneNumberWithPrefix, imageURL: Common.Value.empty, id: Common.Value.empty, firstName: Common.Value.empty, lastName: Common.Value.empty, age: Common.Value.empty)
        createUser.createUser(user: user, onSuccess: { (userInfo) in
                self.dismiss(animated: true, completion: nil)
            }) { (error) in
                // Check if there is an error creating new user
                if let error = error {
                    // Handle Firebase user creation error
                    self.handleFirebaseUserCreationError(error: error)
                }
            }
    }
    
    // Navigation
    
    // MARK: - IBActions
    
    @IBAction func cancelButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Unwind.toLogin, sender: nil)
    }
    
    // Check country code
    func checkCountryCode(countryName name: String?) -> String? {
        // Get country code by name from the database
        if let countryCode = CountryService.getCountryCode(withName: (name)!) {
            // Check if the user entered a valid country name
            guard let code = countryCode.first?.code else {
                // Disable create user button
                createUserButton.isEnabled = false
                return nil
            }
            // Enable create user button
            createUserButton.isEnabled = true
            // Concatenate plus sign "+" and country code
            return UI.TextField.plusSign + code
        }
        // Disable create user button
        createUserButton.isEnabled = false
        return nil
    }
}


// MARK: - Extensions
extension CreateUserViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Country successfully found
        if let countryCode = checkCountryCode(countryName: textField.text?.capitalized) {
            textField.text = countryCode
        // Country not found
        } else {
            handleUserCreationError(forError: UserUI.Create.Error.countryNotFoundError)
            textField.clear()
        }
    }
}
