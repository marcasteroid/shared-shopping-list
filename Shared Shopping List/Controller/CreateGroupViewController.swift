//
//  CreateNewGroupViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Contacts
import Firebase

class CreateGroupViewController: UIViewController {

    // MARK: - IBOutlets
    // Users group collection view
    @IBOutlet weak var usersGroupCollectionView: UICollectionView!
    // Contacts table view
    @IBOutlet weak var contactsTableView: UITableView!
    // Done button
    @IBOutlet weak var doneButton: CustomButton!
    // Refresh contacts list button
    @IBOutlet weak var refreshContactsListButton: CustomButton!
    // Group empty message label
    @IBOutlet weak var groupEmptyLabel: UILabel!
    // Number of users left
    @IBOutlet weak var numbersOfContactsLeft: UILabel!
    // No contacts info message
    @IBOutlet weak var noContactsInfoMessage: UIStackView!
    
    
    // MARK: - Variables

    // Address book contacts
    private var contacts = [AddressBookContact]()
    
    // Contact phone number array
    var contactPhoneNumber = [String]()
    
    // Contacts with account phone numbers
    private var contactsWithAccountPhoneNumber = [String]()
    
    // Users collection reference
    private var usersWithAccountCollectionReference: CollectionReference!
    
    // Users group array
    private var usersGroup = [UserGroup]()
    
    // Users added to group array
    private var usersAddedToGroup = [UserAddedToGroup]()
    
    // Users in group phone numbers array
    private var usersInGroupPhoneNumbers = [String]()
    
    // User ids
    private var userIds = [String]()
    private var members = [String: String]()
    
    // Logged in user phone number
    private var loggedInUserPhoneNumber: String!
    
    // User with account id
    var userWithAccountId = [String]()
    
    private var chosenUserArray = [String]()
    
    private var usersGroupImage: UIImage? = nil
    private var usersGroupFullName: String!
    
    private var phoneNumberNoCountryCode: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        usersGroupCollectionView.delegate = self
        usersGroupCollectionView.dataSource = self
        
        // Table view delegate
        contactsTableView.delegate = self
        // Table view data source
        contactsTableView.dataSource = self
        // Hide doneButton
        doneButton.isHidden = true
        // Hide number of contacts left
        numbersOfContactsLeft.isHidden = true
        
        //doneButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2.0)
        
        // Initialize users collection reference
        usersWithAccountCollectionReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName)
        
        fetchLoggedInUserPhoneNumber(handler: { (returnedLoggedInUserPhoneNumber) in
            self.loggedInUserPhoneNumber = returnedLoggedInUserPhoneNumber
            
            self.fetchContactPhoneNumber()
            
            self.fetchUsersWithAccountPhoneNumbers(handler: { (returnedPhoneNumbersArray) in
                self.contactsWithAccountPhoneNumber = returnedPhoneNumbersArray
                self.fetchContacts()
            })
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // Init users group
    fileprivate func initUsersGroup() {
        usersGroup = UserGroupService.instance.getUsersGroup()
    }
    
    // Get users id
    func getUserIds(forPhoneNumbers phoneNumbers: [String], handler: @escaping (_ uidArray: [String]) -> ()) {
        var idArray = [String]()
        Firestore.firestore().collection(FirebaseCollection.Users.collectionName).getDocuments { (querySnapshot, error) in
            if let error = error {
                print("Error getting the document: \(error)")
            } else {
                for document in querySnapshot!.documents {
                    let phoneNumber = document.get(Common.Query.phoneNumber)! as! String
                    if phoneNumbers.contains(phoneNumber) {
                        idArray.append(document.documentID)
                    }
                }
                handler(idArray)
            }
        }
    }
    
    
    fileprivate func fetchUsersWithAccountPhoneNumbers(handler: @escaping (_ phoneNumberArray: [String]) -> ()) {
        var phoneNumberArray = [String]()
        Firestore.firestore().collection(FirebaseCollection.Users.collectionName).getDocuments() { (querySnapshot, error) in
            if let error = error {
                print("Error getting the document: \(error)")
            } else {
                for document in querySnapshot!.documents {
                    let phoneNumber = document.get(Common.Query.phoneNumber)! as! String
                    phoneNumberArray.append(phoneNumber)
                    // Filter array removing phone number of the logged in user
                    phoneNumberArray = phoneNumberArray.filter { $0 != self.loggedInUserPhoneNumber }
                }
            }
            handler(phoneNumberArray)
        }
    }
    
    func fetchLoggedInUserPhoneNumber(handler: @escaping (_ userPhoneNumber: String) -> ()) {
        var userPhoneNumber: String!
        // Get user id
        guard let userID = Auth.auth().currentUser?.uid else { return }
        let documentReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID)
        documentReference.getDocument { (document, error) in
            if let document = document, document.exists {
                let phoneNumber = document.get(Common.Query.phoneNumber)! as! String
                userPhoneNumber = phoneNumber
            } else {
                print("Document does not exist")
            }
            handler(userPhoneNumber)
        }
    }
    
    // Fetch contacts
    fileprivate func fetchContacts() {
        // Contact store
        let contactStore = CNContactStore()
        
        // Request access
        contactStore.requestAccess(for: .contacts) { (granted, error) in
            // Failed to request access
            if let error = error {
                print("Failed to request access \(error)")
                return
            }
            
            // Permissions to access contacts list granted
            if granted {
                // Informations about the contact
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactImageDataKey, CNContactImageDataAvailableKey, CNContactPhoneNumbersKey]
                // Contact fetch request
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    try contactStore.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        guard let phoneNumber = contact.phoneNumbers.first?.value.stringValue.digits else { return }
                        if self.contactsWithAccountPhoneNumber.contains(phoneNumber) {
                            // Check if the image data is available
                            if contact.imageDataAvailable {
                                let image = UIImage(data: contact.imageData!)
                                self.contacts.append(AddressBookContact(firstName: contact.givenName, familyName: contact.familyName, image: image!, hasContactImage: true))
                            } else { // Image data not available
                                let image = UIImage(named: Image.Image.Placeholder.whiteBox)!
                                self.contacts.append(AddressBookContact(firstName: contact.givenName, familyName: contact.familyName, image: image, hasContactImage: false))
                            }
                        }
                    })
                } catch let error {
                    print("Failed to enumerate contacts \(error)")
                }
                // The user has no contacts
                if self.contacts.count == 0 {
                    self.noContactsInfoMessage.isHidden = false
                    self.groupEmptyLabel.isHidden = true
                } else {
                    self.noContactsInfoMessage.isHidden = true
                    self.groupEmptyLabel.isHidden = false
                    // Load contacts into the contacts table view
                    OperationQueue.main.addOperation({
                        self.contactsTableView.reloadData()
                    })
                }
            } else {
                print("Not granted")
            }
        }
    }
    
    // Fetch contacts phone numbers
    fileprivate func fetchContactPhoneNumber() {
        // Contact store
        let contactStore = CNContactStore()
        
        // Request access
        contactStore.requestAccess(for: .contacts) { (granted, error) in
            // Failed to request access
            if let error = error {
                print("Failed to request access \(error)")
                return
            }
            
            // Permissions to access contacts list granted
            if granted {
                // Informations about contact phone number
                let keys = [CNContactPhoneNumbersKey]
                // Contact fetch request
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    try contactStore.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        guard let phoneNumber = contact.phoneNumbers.first?.value.stringValue.digits else { return }
                        self.contactPhoneNumber.append(phoneNumber)
                    })
                } catch let error {
                    print("Failed to enumerate contacts \(error)")
                }
            } else {
                print("Not granted")
            }
        }
    }
    
    fileprivate func fetchContactPhoneNumber(forFirstName firstName: String, andLastName lastName: String) -> String? {
        var phoneNumber: String? = nil
        // Contact store
        let contactStore = CNContactStore()
        
        // Request access
        contactStore.requestAccess(for: .contacts) { (granted, error) in
            // Failed to request access
            if let error = error {
                print("Failed to request access \(error)")
            }
            
            // Permissions to access contacts list granted
            if granted {
                // Informations about contact
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    try contactStore.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        if contact.givenName == firstName && contact.familyName == lastName {
                            guard let contactPhoneNumber = contact.phoneNumbers.first?.value.stringValue.digits else { return }
                            phoneNumber = contactPhoneNumber
                            self.usersInGroupPhoneNumbers.append(phoneNumber!)
                            
                            self.getUserIds(forPhoneNumbers: self.usersInGroupPhoneNumbers) { (idsArray) in
                                self.userIds = idsArray
                                // Get current user id
                                guard let userID = Auth.auth().currentUser?.uid else { return }
                                self.userIds.append(userID)
                            }
                        }
                    })
                } catch let error {
                    print("Failed to enumerate contacts \(error)")
                }
            }
        }
        return phoneNumber
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let groupTitleViewController = segue.destination as? GroupTitleViewController else { return }
        groupTitleViewController.userIds = userIds
        groupTitleViewController.users = usersGroup
    }
    
    // MARK: - IBActions
    
    // Back button
    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // Done button
    @IBAction func doneButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Storyboard.groupTitleViewController, sender: nil)
    }
    
    // Refresh contacts list button
    @IBAction func refreshContactsListButtonWasTapped(_ sender: Any) {
        fetchLoggedInUserPhoneNumber(handler: { (returnedLoggedInUserPhoneNumber) in
            self.loggedInUserPhoneNumber.removeAll()
            self.loggedInUserPhoneNumber = returnedLoggedInUserPhoneNumber
            
            self.fetchContactPhoneNumber()
            
            self.fetchUsersWithAccountPhoneNumbers(handler: { (returnedPhoneNumbersArray) in
                self.contactsWithAccountPhoneNumber.removeAll()
                self.contactsWithAccountPhoneNumber = returnedPhoneNumbersArray
                self.fetchContacts()
            })
        })
        //self.contactsTableView.del
    }
    //self.usersGroupCollectionView.reloadData()
}


// MARK: - Extenions

// Collection view extension
extension CreateGroupViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return usersGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.UserGroupCell.identifier, for: indexPath) as? UserGroupCollectionViewCell else { return UserGroupCollectionViewCell() }
        let userGroup = usersGroup[indexPath.row]
        cell.updateView(userGroup: userGroup)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        usersGroup.remove(at: indexPath.row)
        chosenUserArray.remove(at: indexPath.row)
        usersAddedToGroup.remove(at: indexPath.row)
        contactsTableView.cellForRow(at: indexPath)?.isUserInteractionEnabled = true
        numbersOfContactsLeft.text = "\(String(usersGroup.count)) of 20 selected"
        usersGroupCollectionView.reloadData()
        // Check if the user group array is empty
        if usersGroup.count == 0 {
            // Hide group emty label
            groupEmptyLabel.isHidden = false
            // Hide done button
            doneButton.isHidden = true
            // Show refresh button
            refreshContactsListButton.isHidden = false
            // Hide number of contacts left
            numbersOfContactsLeft.isHidden = true
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return CollectionViewCell.numberOfSections
    }
}

// Table view extension
extension CreateGroupViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.AddressBookContacsListCell.identifier, for: indexPath) as? AddressBookContactTableViewCell else { return AddressBookContactTableViewCell() }
        let contact = contacts[indexPath.row]
        
        cell.configureCell(contact: contact)
        // Set cell background to clear
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.AddressBookContacsListCell.identifier) as? AddressBookContactTableViewCell else { return }
        
        // Check if the chosen user array does not contain selected user
        if !chosenUserArray.contains(cell.contactFullName.text!) {
            let selectedUser = contacts[indexPath.row]
            usersGroupFullName = selectedUser.firstName + Common.Value.singleSpace + selectedUser.lastName
            guard let phoneNumber = fetchContactPhoneNumber(forFirstName: selectedUser.firstName, andLastName: selectedUser.lastName) else { return }
            let userAddedToGroup = UserAddedToGroup(firstName: selectedUser.firstName, lastName: selectedUser.lastName, phoneNumber: phoneNumber)
            
            if selectedUser.hasContactImage {
                usersGroupImage = selectedUser.image
            } else {
                usersGroupImage = nil
            }
            
            let newUserInGroup = UserGroup.init(image: usersGroupImage, fullname: usersGroupFullName)
            
            // Selected user is not in the array
            if !usersGroup.contains(newUserInGroup) {
                // Add selected user in the array
                usersGroup.append(newUserInGroup)
                // Add selected user in chosen user array
                chosenUserArray.append(usersGroupFullName)
                // Add selected user is group
                usersAddedToGroup.append(userAddedToGroup)
                contactsTableView.cellForRow(at: indexPath)?.isUserInteractionEnabled = false
                // Show group emty label
                groupEmptyLabel.isHidden = true
                // Show done button
                doneButton.isHidden = false
                // Hide refresh button
                refreshContactsListButton.isHidden = true
                // Show number of contacts left
                numbersOfContactsLeft.isHidden = false
                numbersOfContactsLeft.text = "\(String(usersGroup.count)) of 20 selected"
            }
            // Reload tableview
            usersGroupCollectionView.reloadData()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewCell.numberOfSections
    }
    
}

extension RangeReplaceableCollection where Self: StringProtocol {
    var digits: Self {
        return filter({ $0.isNumber })
    }
}

extension Array where Element: Hashable {
    func intersection(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.intersection(otherSet))
    }
}
