//
//  TestViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 30/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func buttonWasTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: ViewController.tabBarController)
        self.present(tabBarController, animated: true, completion: nil) 
    }
}
