//
//  LoginViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
import TwitterKit
import RealmSwift
import JGProgressHUD

class LoginViewController: UIViewController/*, GIDSignInUIDelegate*/ {
    
    // MARK: - Properties
    
    // StandardLoginService
    var standardLoginService = StandardLoginService()
    // NetworkService
    var networkService = NetworkService()
    
    // MARK: - IBOutlets
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var emailPassword: UIStackView!
    @IBOutlet weak var headerTitle: UIStackView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var signUpButton: CustomButton!
    @IBOutlet weak var createUserButton: CustomButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        // Hide navigation bar bottom line
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        //detectDevice()
        // Setup view
        setupView()
        
        // Google sign in delegate
        //GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
    }
    /*
    func detectDevice() {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S of 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X")
            case 2688:
                print("iPhone XS Max")
            case 1792:
                print("iPhone XR")
                self.welcomeLabel.font = self.welcomeLabel.font.withSize(40)
                self.titleLabel.font = self.titleLabel.font.withSize(60)
                
                self.headerView.translatesAutoresizingMaskIntoConstraints = false
                self.headerTitle.translatesAutoresizingMaskIntoConstraints = false
                self.emailPassword.translatesAutoresizingMaskIntoConstraints = false
                
                let constraint_1 = NSLayoutConstraint(item: headerView, attribute: .bottom, relatedBy: .equal, toItem: emailPassword, attribute: .top, multiplier: 1, constant: -50)
                let constraint_2 = NSLayoutConstraint(item: emailPassword, attribute: .bottom, relatedBy: .equal, toItem: mainView, attribute: .bottom, multiplier: 1, constant: -270)
                let constraint_3 = NSLayoutConstraint(item: headerTitle, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1, constant: 250)
                let constraint_4 = NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: welcomeLabel, attribute: .bottom, multiplier: 1, constant: 20)
                
                mainView.addConstraint(constraint_1)
                mainView.addConstraint(constraint_2)
                mainView.addConstraint(constraint_3)
                mainView.addConstraint(constraint_4)
            default:
                print("Unkown device")
            }
        }
    }
    */
    // MARK: - Setup view
    func setupView() {
        // Hide keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.handleTap))
        view.addGestureRecognizer(tap)
        
        // Listen for keyboard events
        // Keyboard will show
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        // Keyboard will hide
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Keyboard will change frame
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Remove notifications
    deinit {
        // Stop listening for keyboard hide/show events
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    // Keyboard will change
    @objc func keyboardWillChange(notification: Notification) {
        // Keyboard rectangle
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        // Get bar height
        //let barHeight = self.navigationController?.navigationBar.frame.height ?? 0
        // Get status bar height
        //let statusBarHeight = UIApplication.shared.isStatusBarHidden ? CGFloat(0) : UIApplication.shared.statusBarFrame.height
        // Calculate distance from the top border
        //let topDistance = barHeight + statusBarHeight
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            // Hide navigation controller
            //self.navigationController?.navigationBar.isHidden = true
            // Hide header view
            self.headerView.isHidden = true
            // Hide create user button
            self.createUserButton.isHidden = true
            //view.frame.origin.y = -(keyboardRect.height - topDistance)
            // Set frame y origin
            view.frame.origin.y = -keyboardRect.height
        } else {
            // Set navigation controller visible
            //self.navigationController?.navigationBar.isHidden = false
            // Set header view visible
            self.headerView.isHidden = false
            // Set create user button visible
            self.createUserButton.isHidden = false
            view.frame.origin.y = 0
        }
    }
    
    // MARK: - IBActions
    
    // Login
    @IBAction func loginButtonWasTapped(_ sender: Any) {
        // Check if the email text field is not empty
        guard let email = emailTextField.text, email.isNotEmpty else {
            // Handle authentication error for email errors
            handleUserAuthenticationError(forError: UserUI.Authentication.Error.emailTextFieldError)
            return
        }
        // Check if the password text field is not empty
        guard let password = passwordTextField.text, password.isNotEmpty else {
            // Handle authentication error for password errors
            handleUserAuthenticationError(forError: UserUI.Authentication.Error.passwordTextFieldError)
            return
        }
        
        // Show progress
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "Logging in"
        hud.textLabel.font = UIFont(name: "Avenir-Medium", size: 15)
        hud.show(in: self.view)
        
        // Authenticate the user
        standardLoginService.loginWithEmailAndPassword(email: email, andPassword: password, onSuccess: { (_) in
            NotificationCenter.default.post(name: Notification.Name(Message.StandardLogin.Alert.Notification.userDataDidChange), object: nil)
            // Hide the software keyboard
            self.view.endEditing(true)
            // Clear username textfield
            self.emailTextField.clear()
            // Clear password text field
            self.passwordTextField.clear()
            // Present the shopping list view controller
            let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
            let tabBarController = storyboard.instantiateViewController(withIdentifier: ViewController.tabBarController)
            self.present(tabBarController, animated: true, completion: nil)
        }) { (error) in
            if let error = error { // Check if there is an error with login
                // Handle firebase authentication error
                self.handleFirebaseAuthError(error: error)
            }
        }
    }
    
    // Create user button was tapped
    @IBAction func createUserButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Storyboard.createUserViewController, sender: nil)
    }
    
    // Forgot password button was tapped
    @IBAction func forgotPasswordButtonWasTapped(_ sender: Any) {
        let viewController = ForgotPasswordViewController()
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) { /* Empty */ }
    
    // MARK: - Social login
    
    // Google login
    @IBAction func googleLoginButtonWasTapped(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    // Facebook login
    @IBAction func facebookLoginButtonWasTapped(_ sender: Any) {
        // TODO
    }
    
    // Twitter login
    @IBAction func twitterLoginButtonWasTapped(_ sender: Any) {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            // Check for errors
            if let error = error {
                debugPrint("Cannot login with Twitter: \(error.localizedDescription)")
            }
            // No errors occourred
            if let session = session {
                let credential = TwitterAuthProvider.credential(withToken: session.authToken, secret: session.authTokenSecret)
                self.firebaseLogin(credential)
            }
        }
    }
    
    func firebaseLogin(_ credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (user, error) in
            // Check for errors
            if let error = error {
                debugPrint("Firebase login error: \(error.localizedDescription)")
                return
            } else {
                print("Firebase login success")
                // Present the shopping list view controller
                let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
                let tabBarController = storyboard.instantiateViewController(withIdentifier: ViewController.tabBarController)
                self.present(tabBarController, animated: true, completion: nil)
            }
        }
    }
    
}
