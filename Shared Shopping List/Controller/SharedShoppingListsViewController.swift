//
//  SharedShoppingListsViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 31/05/2020.
//  Copyright © 2020 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import JGProgressHUD

class SharedShoppingListsViewController: UIViewController {
    
    // Search bar
    @IBOutlet weak var searchBar: UISearchBar!
    // Shared shopping lists table view
    @IBOutlet weak var sharedShoppingListsTableView: UITableView!
    // No shopping lists info message
    @IBOutlet weak var noShoppingListsInfoMessage: UIStackView!
    // Shared shopping lists listener
    private var sharedShoppingListsListener: ListenerRegistration!
    // Shared Shopping lists document reference
    private var sharedShoppingListReference: DocumentReference!
    // Array of shared shopping lists
    private var sharedShoppingLists = [ShoppingList]()
    // Authentication handler
    private var handle: AuthStateDidChangeListenerHandle?
    // Shared shopping list
    private var sharedShoppingList: ShoppingList!
    // Array of shared shopping list ID
    var sharedShoppingListsID = [String]()
    // Shared shopping list service
    private var sharedShoppingListsService = ShoppingListsService()
    // Filtered shared shopping lists
    private lazy var filteredSharedShoppingLists = sharedShoppingLists
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Table view delegate
        sharedShoppingListsTableView.delegate = self
        // Table view data source
        sharedShoppingListsTableView.dataSource = self
        // Search bar delegate
        searchBar.delegate = self
        
        // Do any additional setup after loading the view.
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .selected)
                
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 13)!]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes
        
        searchBar.isHidden = true
        
        getGroupsId { (returnedGroupIdArray) in
            for groupID in returnedGroupIdArray {
                self.getSharedShoppingLists(groupID: groupID)
            }
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
//            // Check if the user is logged in
//            if user == nil {
//                self.dismiss(animated: true, completion: nil)
//            } else { // User logged in
//                self.getGroupsId { (returnedGroupIdArray) in
//                    for groupID in returnedGroupIdArray {
//                        self.getSharedShoppingLists(groupID: groupID)
//                    }
//                }
//            }
//        })
//    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredSharedShoppingLists = sharedShoppingLists.filter { (sharedShoppingList: ShoppingList) -> Bool in
            return sharedShoppingList.name.lowercased().contains(searchText.lowercased())
        }
        sharedShoppingListsTableView.reloadData()
    }
    
    // Get groups id
    fileprivate func getGroupsId(handler: @escaping (_ groupsIdArray: [String]) -> ()) {
        var groupsIdArray = [String]()
        guard let userID = Auth.auth().currentUser?.uid else { return }
        // Get personal groups
        Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.Groups.collectionName).getDocuments { (querySnapshot, error) in
            if let error = error {
                debugPrint("Error getting documents: \(error)")
            } else {
                for document in querySnapshot!.documents {
                    let groupID = document.documentID
                    self.getSharedShoppingLists(groupID: groupID)
                }
            }
            handler(groupsIdArray)
        }
    }
    
    // Get shared shopping lists
    func getSharedShoppingLists(groupID: String) {
        // Snapshot listner for shopping lists collection and order by date of creation in descending order (most recent)
        //guard let currentUserID = Auth.auth().currentUser?.uid else { return }
        sharedShoppingListsListener = Firestore.firestore()
            .collection(FirebaseCollection.Groups.collectionName)                        // "groups"
            .document(groupID)                                                          // "groupID"
            .collection(groupID)                                                        // "groupID"
            .document(groupID)
            .collection(groupID)
            //.document(groupID)
            .order(by: FirebaseCollection.ShoppingLists.timestamp, descending: true)    // order by the most recent
            //.whereField(FirebaseCollection.ShoppingLists.shared, isEqualTo: false)
            //.whereField(FirebaseCollection.ShoppingLists.completed, isEqualTo: false)
            .addSnapshotListener { (snapshot, error) in
                // Check for errors
                if let error = error {
                    debugPrint("Error fetching documents \(error.localizedDescription)")
                } else {
                    snapshot?.documentChanges.forEach({ (change) in
                        let data = change.document.data()
                        let sharedShoppingList = ShoppingList.init(data: data)
                        
                        switch change.type {
                        case .added:
                            self.onDocumentAdded(change: change, sharedShoppingList: sharedShoppingList)
                        case .modified:
                            self.onDocumentModified(change: change, sharedShoppingList: sharedShoppingList)
                        case .removed:
                            self.onDocumentRemoved(change: change)
                        @unknown default:
                            fatalError()
                        }
                    })
                    // Check if the user has no shopping lists
                    if self.sharedShoppingLists.count == 0 {
                        self.noShoppingListsInfoMessage.isHidden = false
                        self.searchBar.isHidden = true
                    } else { // The user have shopping lists
                        self.noShoppingListsInfoMessage.isHidden = true
                        self.searchBar.isHidden = false
                    }
                }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func addNewSharedShoppingListButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Storyboard.newSharedShoppingListViewController, sender: nil)
    }
    
}

// MARK: - Extensions

extension SharedShoppingListsViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    // Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewCell.numberOfSections
    }
    
    // Number of rows in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredSharedShoppingLists.count > 0 {
            return filteredSharedShoppingLists.count
        }
        return sharedShoppingLists.count
    }
    
    
    // Height for row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCell.height)
    }
    
    // Cell creation
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.ShoppingLists.identifier, for: indexPath) as? ShoppingListTableViewCell {
//            cell.configureCell(shoppingList: sharedShoppingLists[indexPath.row])
//            return cell
//        } else {
//            return UITableViewCell()
//        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.ShoppingLists.identifier, for: indexPath) as! ShoppingListTableViewCell
        //let shoppingList: ShoppingList
        if filteredSharedShoppingLists.count > 0 {
            sharedShoppingList = filteredSharedShoppingLists[indexPath.row]
        } else {
            sharedShoppingList = sharedShoppingLists[indexPath.row]
        }
        cell.configureCell(shoppingList: sharedShoppingList)
        return cell
    }
    
    /*
     // Override to support conditional editing of the table view.
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    
    // Override to support rearranging the table view.
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) { }
    
    
    // Override to support conditional rearranging of the table view.
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
    // MARK: - Navigation
    
    /*
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if let itemsViewController = segue.destination as? ItemsListTableViewController {
     itemsViewController.initItems(shoppingList: sender as! ShoppingList)
     }
     }
     */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Segue.Storyboard.addItemViewController, sender: sharedShoppingLists[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.Storyboard.addItemViewController {
            if let destinationViewController = segue.destination as? AddItemViewController {
                if let shoppingList = sender as? ShoppingList {
                    destinationViewController.shoppingList = shoppingList
                }
            }
        } else if segue.identifier == Segue.Storyboard.selectGroupViewController {
            if let destinationViewController = segue.destination as? SelectGroupViewController {
                if let shoppingListsID = sender as? [String] {
                    self.sharedShoppingListsID.removeAll()
                    destinationViewController.shoppingListsID = shoppingListsID
                }
            }
        }
    }
    
    // MARK: - Table view swipe actions
    
    // Leading Swipe action
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let width: CGFloat = 25
        let height: CGFloat = 25
        
        // Done action: DA RIFARE
        let done = UIContextualAction(style: .normal, title: nil) { (action, view, nil) in
            // Get user id
            guard let userID = Auth.auth().currentUser?.uid else { return }
            self.sharedShoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.sharedShoppingLists[indexPath.row].documentID)
            // Set selected shopping list as complete
            self.sharedShoppingListsService.setAsCompleted(completed: true, shoppingList: self.sharedShoppingLists[indexPath.row], shoppingListReference: self.sharedShoppingListReference)
            // Remove selected shopping list from the shopping list array
            self.sharedShoppingLists.remove(at: indexPath.row)
            // Delete selected shopping list from the table view
            self.sharedShoppingListsTableView.deleteRows(at: [indexPath], with: .fade)
            self.sharedShoppingListsTableView.reloadData()
        }
        
        let doneImage = UIGraphicsImageRenderer(size: CGSize(width: width, height: height)).image { (context) in
            context.stroke(CGRect(x: 0, y: 0, width: width, height: height), blendMode: .clear)
            ItemActionIcon.drawDoneActionCanvas(frame: CGRect(x: 0, y: 0, width: width, height: height), resizing: .aspectFit)
        }
        done.image = doneImage
        done.backgroundColor = .white
        
        let config = UISwipeActionsConfiguration(actions: [done])
        config.performsFirstActionWithFullSwipe = false
        self.sharedShoppingListsTableView.isEditing = false
        
        return config
    }
    
    // Trailing swipe action
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let width: CGFloat = 23
        let height: CGFloat = 23
        
        // Add to preferred action: DA RIFARE
        let addToPreferred = UIContextualAction(style: .normal, title: nil) { (action, view, nil) in
            // Get user id
            guard let userID = Auth.auth().currentUser?.uid else { return }
            self.sharedShoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.sharedShoppingLists[indexPath.row].documentID)
            // Set selected shopping list as complete
            self.sharedShoppingListsService.setAsSaved(saved: true, shoppingList: self.sharedShoppingLists[indexPath.row], shoppingListReference: self.sharedShoppingListReference)
        }
        
        let addToPreferredImage = UIGraphicsImageRenderer(size: CGSize(width: width, height: height)).image { context in
            context.fill(CGRect(origin: .zero, size: CGSize(width: width, height: height)), blendMode: .clear)
            ItemActionIcon.drawAddToPreferredActionCanvasNotActive(frame: CGRect(x: 0, y: 0, width: width, height: height))
        }
        addToPreferred.image = addToPreferredImage
        addToPreferred.backgroundColor = .white
        
        let config = UISwipeActionsConfiguration(actions: [addToPreferred])
        config.performsFirstActionWithFullSwipe = false
        sharedShoppingListsTableView.isEditing = false
        
        return config
    }
    
    func onDocumentAdded(change: DocumentChange, sharedShoppingList: ShoppingList) {
        let newIndex = Int(change.newIndex)
        sharedShoppingLists.insert(sharedShoppingList, at: newIndex)
        sharedShoppingListsTableView.insertRows(at: [IndexPath(row: newIndex, section: 0)], with: .automatic)
    }
    
    func onDocumentModified(change: DocumentChange, sharedShoppingList: ShoppingList) {
        // Shopping list changed but remaind in the same position
        if change.newIndex == change.oldIndex {
            let index = Int(change.newIndex)
            sharedShoppingLists[index] = sharedShoppingList
            sharedShoppingListsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        } else { // Item changed and changed position
            let oldIndex = Int(change.oldIndex)
            let newIndex = Int(change.newIndex)
            sharedShoppingLists.remove(at: oldIndex)
            sharedShoppingLists.insert(sharedShoppingList, at: newIndex)
            sharedShoppingListsTableView.moveRow(at: IndexPath(row: oldIndex, section: 0), to: IndexPath(row: newIndex, section: 0))
        }
    }
    
    func onDocumentRemoved(change: DocumentChange) {
        let oldIndex = Int(change.oldIndex)
        sharedShoppingLists.remove(at: oldIndex)
        sharedShoppingListsTableView.deleteRows(at: [IndexPath(row: oldIndex, section: 0)], with: .left)
    }
}

extension SharedShoppingListsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredSharedShoppingLists = sharedShoppingLists.filter { (sharedShoppingList: ShoppingList) -> Bool in
            return sharedShoppingList.name.lowercased().contains(searchText.lowercased())
        }
        sharedShoppingListsTableView.reloadData()
    }
}
