//
//  HomeViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //checkUserStatus()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkUserStatus()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Present login view controller
    fileprivate func presentLoginViewController() {
        let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: ViewController.loginViewController)
        present(viewController, animated: true, completion: nil)
    }

    // Check if the user is logged in
    fileprivate func isUserLoggedIn() -> Bool {
        var userLoggedIn: Bool = false
        // The user is logged in
        if let _ = Auth.auth().currentUser { userLoggedIn = true }
        return userLoggedIn
    }
    
    fileprivate func performlogout() {
        do {
            // Perform logout
            try Auth.auth().signOut()
        } catch let error {
            debugPrint(error.localizedDescription)
        }
    }
    
    fileprivate func checkUserStatus() {
        // The user is logged in
        if isUserLoggedIn() {
            // Perform logout
            performlogout()
            // Present login view controller
            presentLoginViewController()
        } else {
            // Present login view controller
            presentLoginViewController()
        }
    }
}
