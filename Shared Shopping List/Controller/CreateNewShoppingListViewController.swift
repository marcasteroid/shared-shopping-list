//
//  CreateNewShoppingListViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase

class CreateNewShoppingListViewController: UIViewController {

    // MARK: - IBOutlets
    // Shopping list name text field
    @IBOutlet weak var shoppingListNameTextField: UITextField!
    // Shopping list tags text field
    @IBOutlet weak var tagsTextField: UITextField!
    // Shopping list tags collection view
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    // Flow layout
    @IBOutlet weak var flowLayout: FlowLayout!
    // Header view
    @IBOutlet weak var headerView: UIView!
    // Create new shopping list button
    @IBOutlet weak var createNewShoppingListButton: CustomButton!
    
    // MARK: - Properties
    // Tag cell size
    var sizingCell: TagCollectionViewCell?
    // Tags
    var tags = [Tag]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup view
        setupView()
        
        // Set Tag collection view delegate
        tagsCollectionView.delegate = self
        // Set Tag collection view datasource
        tagsCollectionView.dataSource = self
        // Set text field delegate
        tagsTextField.delegate = self
        
        tagsTextField.addTarget(self, action: #selector(CreateNewShoppingListViewController.checkDuplicates(_:)), for: UIControl.Event.editingChanged)

        self.tagsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        // Register Nib for Tag collection view cell
        let tagCollectionViewCellNib = UINib(nibName: "TagCollectionViewCell", bundle: nil)
        self.tagsCollectionView.register(tagCollectionViewCellNib, forCellWithReuseIdentifier: "TagCollectionViewCell")
        self.tagsCollectionView.backgroundColor = UIColor.clear
        self.sizingCell = (tagCollectionViewCellNib.instantiate(withOwner: nil, options: nil) as NSArray).firstObject as! TagCollectionViewCell?
        self.flowLayout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    // MARK: - Setup view
    func setupView() {
        // Hide keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(CreateNewShoppingListViewController.handleTap))
        view.addGestureRecognizer(tap)
        
        tap.cancelsTouchesInView = false
        
        // Listen for keyboard events
        // Keyboard will show
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        // Keyboard will hide
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Keyboard will change frame
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Keyboard will change
    @objc func keyboardWillChange(notification: Notification) {
        // Keyboard rectangle
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            // Hide the header view
            self.headerView.isHidden = true
            // Hide create new shopping list button
            self.createNewShoppingListButton.isHidden = true
            // Set frame y origin
            view.frame.origin.y = -keyboardRect.height
            
        } else {
            // Set header view visible
            self.headerView.isHidden = false
            // Set create new shopping list button visible
            self.createNewShoppingListButton.isHidden = false
            // Reset frame y origin
            view.frame.origin.y = 0
        }
    }
    
    // Remove notifications
    deinit {
        // Stop listening for keyboard hide/show events
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Handle tap
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    /*
    // Check for duplicate entries
    @objc func checkDuplicates(_ textField: UITextField) {
        guard let text = textField.text else { return }
        var seen = [String]()
        let words = text.components(separatedBy: " ").filter ({ word in
            if word == "" || seen.contains(word) {
                return false
            } else {
                seen.append(word)
                return true
            }
        })
        var result = words.joined(separator: " ")
        if text.last == " " {
            result += " "
        }
        textField.text = result
    }
     */
    
    @objc func checkDuplicates(_ textField: UITextField) {
        guard let text = textField.text?.lowercased() else { return }
        var finishedWords = text.components(separatedBy: " ").filter({ $0 != "" })
        var endOfResult = ""
        
        if text.last == " " {
            endOfResult = " "
        } else if !finishedWords.isEmpty {
            if finishedWords.count > 1 {
                endOfResult += " "
            }
            endOfResult += finishedWords.last ?? ""
            finishedWords.removeLast()
        }
        
        var seen = [String]()
        let uniqueWords = finishedWords.filter({ word in
            if seen.contains(word) {
                return false
            } else {
                seen.append(word)
                return true
            }
        })
        
        let result = uniqueWords.joined(separator: " ")  + endOfResult
        textField.text = result
    }
    
    func createTag(withName tagName: String, _ textField: UITextField) {
        let tag = Tag()
        tag.name = tagName
        tag.name = textField.text?.components(separatedBy: " ").last?.lowercased()
        self.tags.append(tag)
        self.tags.removeDuplicates()
        tagsCollectionView.reloadData()
    }
    
    // Create personal shopping list
    fileprivate func createPersonalShoppingList() {
        // Check if the shoppinglist text field is not empty
        guard let shoppingListName = shoppingListNameTextField.text, shoppingListName.isNotEmpty else {
            // Handle missing shopping list name
            handleShoppingListCreationError(forError: ShoppingListCreation.CreateNewShoppingList.Error.shoppingListNameTextFieldError)
            return
        }
        
        // Check if the tags text field is not empty
        guard let tags = tagsTextField.text, tags.isNotEmpty else {
            // Handle missing shopping list tags
            handleShoppingListCreationError(forError: ShoppingListCreation.CreateNewShoppingList.Error.shoppingListTagsTextFieldError)
            return
        }
        
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        // Firestore transaction
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            
            let shoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document()
            
            transaction.setData([
                FirebaseCollection.ShoppingLists.name : shoppingListName,
                FirebaseCollection.ShoppingLists.numberOfItems: 0,
                FirebaseCollection.ShoppingLists.tags : tags,
                FirebaseCollection.ShoppingLists.timestamp : Timestamp(),
                FirebaseCollection.ShoppingLists.shared: false,
                FirebaseCollection.ShoppingLists.completed: false,
                FirebaseCollection.ShoppingLists.saved: false,
                FirebaseCollection.ShoppingLists.dateCompleted : Timestamp(),
                FirebaseCollection.ShoppingLists.adminID: userID,
                FirebaseCollection.ShoppingLists.id: shoppingListReference.documentID], forDocument: shoppingListReference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                self.shoppingListNameTextField.clear()
                self.shoppingListNameTextField.resignFirstResponder()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    // MARK: - IBActions
    @IBAction func createNewShoppingListButtonWasTapped(_ sender: Any) {
        createPersonalShoppingList()
    }
    
    
    // MARK: - Navigation

    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Extensions

extension CreateNewShoppingListViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as! TagCollectionViewCell
        self.configureCell(cell: cell, forIndexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.configureCell(cell: self.sizingCell!, forIndexPath: indexPath)
        //self.sizingCell?.setNeedsLayout()
        self.sizingCell?.layoutIfNeeded()
        return self.sizingCell!.systemLayoutSizeFitting(UICollectionView.layoutFittingCompressedSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //collectionView.deselectItem(at: indexPath, animated: false)
        let tagToRemove = String(tags[indexPath.row].name!)
        // TODO: Consider to use guard let...
        tagsTextField.text = tagsTextField.text?.replacingOccurrences(of: tagToRemove + " ", with: "", options: [.caseInsensitive])
        tags.remove(at: indexPath.row)
        collectionView.reloadData()
    }
    
    func configureCell(cell: TagCollectionViewCell, forIndexPath indexPath: IndexPath) {
        let tag = tags[indexPath.row]
        cell.tagName.text = tag.name
    }
}

extension CreateNewShoppingListViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            guard let tagName = textField.text, tagName.isNotEmpty else {
                // Handle missing shopping list tags
                handleShoppingListCreationError(forError: ShoppingListCreation.CreateNewShoppingList.Error.shoppingListTagsTextFieldError)
                return false
            }
            
            // Check if the user tap the space bar without entering a tag
            guard textField.text!.suffix(1) != " " else {
                // Handle missing shopping list tags
                handleShoppingListCreationError(forError: ShoppingListCreation.CreateNewShoppingList.Error.shoppingListTagsTextFieldError)
                return false
            }
            
            //tagsTextField.addTarget(self, action: #selector(CreateNewShoppingListViewController.checkDuplicates(_:)), for: UIControl.Event.editingChanged)
            
            // Create tag object
            createTag(withName: tagName, textField)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let tags = textField.text, tags.isNotEmpty else { return }
        textField.text?.append("")
    }
}

extension CreateNewShoppingListViewController: PassDataBetweenViewControllers {
    
    func passData(data: Any) {
        shoppingListNameTextField.text = data as? String
    }
}

extension Array where Element: Equatable {
    mutating func removeDuplicates() {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        self = result
    }
}
