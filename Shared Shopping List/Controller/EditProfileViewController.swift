//
//  EditProfileViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 22/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage
import Kingfisher

class EditProfileViewController: UIViewController {

    // MARK: - IBOutlets
    // User image
    @IBOutlet weak var userProfileImage: CustomImage!
    // Edit icon image
    @IBOutlet weak var editIconImage: UIImageView!
    // Username
    @IBOutlet weak var usernameLabel: UILabel!
    // First name
    @IBOutlet weak var firstNameTextField: UITextField!
    // Last name
    @IBOutlet weak var lastNameTextField: UITextField!
    // Country name
    @IBOutlet weak var countryNameTextField: UITextField!
    // Phone number
    @IBOutlet weak var phoneNumberTextField: UITextField!
    // Age
    @IBOutlet weak var ageTextField: UITextField!
    // Edit user button
    @IBOutlet weak var editUserButton: CustomButton!
    // First username letter
    @IBOutlet weak var firstLetterUsernameLabel: UILabel!
    
    
    // MARK: - Variables
    var listener: ListenerRegistration!
    // Edit user profile service
    var editUserProfile = EditUserProfileService()
    // Create a storage image reference
    var imageReference: StorageReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // User image tap gesture recognizer
        let userImageTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(userImageTapped(tapGestureRecognizer:)))
        userImageTapGestureRecognizer.numberOfTapsRequired = 1
        userProfileImage.isUserInteractionEnabled = true
        userProfileImage.addGestureRecognizer(userImageTapGestureRecognizer)
        
        // Edit icon image gesture recognizer
        let editIconImageTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(userImageTapped(tapGestureRecognizer:)))
        editIconImageTapGestureRecognizer.numberOfTapsRequired = 1
        editIconImage.isUserInteractionEnabled = true
        editIconImage.addGestureRecognizer(editIconImageTapGestureRecognizer)
        
        countryNameTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupUIForSignedInUser()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        listener.remove()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Upload to Storage

    // Upload user image to Storage
    func uploadUserProfileImage() {
        guard let image = userProfileImage.image else {
            print("Must add user image")
            return
        }
        var userReference: DocumentReference!
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        // Turn the image into data
        guard let imageData = image.jpegData(compressionQuality: 0.1) else { return }
        // Create a storage image reference
        let imageReference = Storage.storage().reference().child("/usersProfileImages/\(userID).jpg")
        // Set meta data
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        // Upload data
        imageReference.putData(imageData, metadata: metaData) { (storageMetaData, error) in
            if let error = error {
                print("Put data error: \(error.localizedDescription)")
                return
            }
            // Retrieve download image URL
            imageReference.downloadURL(completion: { (url, error) in
                if let error = error {
                    print("Download URL error: \(error.localizedDescription)")
                    return
                }
                guard let url = url else { return }
                
                userReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID)
                
                self.editUserProfile.updateProfileImage(url: url.absoluteString, usersReference: userReference)
            })
        }
    }

    // Upload document
    func updateUserInfo() {
        
        guard let firstName = firstNameTextField.text, firstName.isNotEmpty else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.firstNameTextFieldError)
            return
        }
        
        guard let lastName = lastNameTextField.text, lastName.isNotEmpty else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.lastNameTextFieldError)
            return
        }
        
        guard let countryName = countryNameTextField.text, countryName.isNotEmpty else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.countryTextFieldError)
            return
        }
        
        guard let phoneNumber = phoneNumberTextField.text, phoneNumber.isNotEmpty else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.phoneNumberTextFieldError)
            return
        }
        
        guard let invalidPhoneNumber = phoneNumberTextField.text, invalidPhoneNumber.isNotValid else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.phoneNumberBadlyFormattedError)
            return
        }
        
        guard let age = ageTextField.text, age.isNotEmpty else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.ageTextFieldError)
            return
        }
        
        guard ageTextField.text!.containsOnlyDigits else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.ageTextFieldContainsCharactersError)
            ageTextField.clear()
            return
        }
        
        let phoneNumberWithPrefix = countryName + Common.Value.countryCodeSeparator + phoneNumber
        
        var userReference: DocumentReference!
        
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        userReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID)
        
        editUserProfile.updateInfo(firstName: firstName, lastName: lastName, age: age, phoneNumber: phoneNumber, phoneNumberWithPrefix: phoneNumberWithPrefix, usersReference: userReference)
    }
    
    // MARK: - Setup View
    
    // Set up the user interface when the user is logged in
    func setupUIForSignedInUser() {
        usernameLabel.text = Auth.auth().currentUser?.displayName?.capitalized
        getUserProfileImage()
        getUserProfileInfoThenUpdateUI()
    }
    
    func getUserProfileImage() {
        guard let userID = Auth.auth().currentUser?.uid else { return }
        let documentReference = Firestore.firestore().collection(FirebaseCollection.Users.usersReference).document(userID)
        
        listener = documentReference.addSnapshotListener { (documentSnapshot, error) in
            if let error = error {
                print("Add snapshot listener error: \(error.localizedDescription)")
            }
            guard let data = documentSnapshot?.data() else { return }
            let imageURL = data[FirebaseCollection.Users.imageURL] as? String ?? Common.Value.empty
            if let url = URL(string: imageURL) {
                self.firstLetterUsernameLabel.isHidden = true
                let options: KingfisherOptionsInfo = [KingfisherOptionsInfoItem.transition(.fade(0.1)), .scaleFactor(UIScreen.main.scale)]
                self.userProfileImage.kf.indicatorType = .activity
                self.userProfileImage.kf.setImage(with: url, options: options)
            } else {
                guard let firstLetter = Auth.auth().currentUser?.displayName?.capitalized.first else {
                    self.firstLetterUsernameLabel.text = Common.Value.empty
                    return
                }
                self.firstLetterUsernameLabel.text = String(firstLetter)
            }
        }
    }

    func getUserProfileInfoThenUpdateUI() {
        guard let userID = Auth.auth().currentUser?.uid else { return }
        let documentReference = Firestore.firestore().collection(FirebaseCollection.Users.usersReference).document(userID)
        
        listener = documentReference.addSnapshotListener { (documentSnapshot, error) in
            if let error = error {
                print("Add snapshot listener error: \(error.localizedDescription)")
            }
            guard let data = documentSnapshot?.data() else { return }
            self.firstNameTextField.text = data[FirebaseCollection.Users.firstName] as? String ?? Common.Value.empty
            self.lastNameTextField.text = data[FirebaseCollection.Users.lastName] as? String ?? Common.Value.empty
            let phoneNumberWithPrefix = data[FirebaseCollection.Users.phoneNumberWithPrefix] as? String ?? Common.Value.empty
            self.countryNameTextField.text = self.getCountryCode(phoneNumberWithPrefix: phoneNumberWithPrefix)
            self.phoneNumberTextField.text = data[FirebaseCollection.Users.phoneNumber] as? String ?? Common.Value.empty
            self.ageTextField.text = data[FirebaseCollection.Users.age] as? String ?? Common.Value.empty
        }

    }
    
    func getCountryCode(phoneNumberWithPrefix: String) -> String {
        if let countryCodeSeparator = phoneNumberWithPrefix.firstIndex(of: Character(Common.Value.countryCodeSeparator)) {
            return String(phoneNumberWithPrefix[phoneNumberWithPrefix.startIndex...phoneNumberWithPrefix.index(before: countryCodeSeparator)])
            
        }
        return Common.Value.empty
    }
    
    // Check country code
    func checkCountryCode(countryName name: String?) -> String? {
        // Get country code by name from the database
        if let countryCode = CountryService.getCountryCode(withName: (name)!) {
            // Check if the user entered a vaild country code
            guard let code = countryCode.first?.code else {
                // Disable edit user button
                editUserButton.isEnabled = false
                return nil
            }
            // Enable edit user button
            editUserButton.isEnabled = true
            // Concatenate plus sign "+" and country code
            return UI.TextField.plusSign + code
        }
        // Disable edit user button
        editUserButton.isEnabled = false
        return nil
    }
    
    // MARK: - IBActions
    
    @objc func userImageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        // Launch image picker
        launchImagePicker()
    }
    
    @IBAction func editButtonWasTapped(_ sender: Any) {
        updateUserInfo()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func launchImagePicker() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        userProfileImage.contentMode = .scaleAspectFill
        userProfileImage.image = image
        uploadUserProfileImage()
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // Country successfully found
        if let countryCode = checkCountryCode(countryName: textField.text?.capitalized) {
            textField.text = countryCode
        } else {
            handleEditUserProfileError(forError: UserUI.Edit.Error.countryNotFoundError)
            textField.clear()
        }
    }
}
