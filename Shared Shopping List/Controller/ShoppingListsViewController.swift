//
//  ShoppingListsViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 03/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import JGProgressHUD

class ShoppingListsViewController: UIViewController {

    // MARK: - IBOutlets
    
    // Search bar
    @IBOutlet weak var searchBar: UISearchBar!
    // Shopping lists table view
    @IBOutlet weak var shoppingListsTableView: UITableView!
    // No shopping lists info message
    @IBOutlet weak var noShoppingListsInfoMessage: UIStackView!
    
    
    // MARK: - Variables
    // Shopping list service
    private var shoppingListsService = ShoppingListsService()
    // Shopping lists collection reference
    private var shoppingListsCollectionReference: CollectionReference!
    // Shopping lists document reference
    private var shoppingListReference: DocumentReference!
    // Shopping list listner
    private var shoppingListsListener: ListenerRegistration!
    // Shared shopping lists document reference
    private var sharedShoppingListReference: DocumentReference!
    // Shared shopping lists listener
    private var sharedShoppingListsListener: ListenerRegistration!
    // Array of shopping lists
    private var shoppingLists = [ShoppingList]()
    // Shopping list
    private var shoppingList: ShoppingList!
    // Array of shopping list ID
    var shoppingListsID = [String]()
    // Groups ID
    var groupsID = [String]()
    // Authentication handler
    private var handle: AuthStateDidChangeListenerHandle?
    // Shared shopping list document ID
    var document: String!
    // Shared shopping list admin user ID
    private var sharedShoppingListAdminID: String!
    // Shared shopping lists ID
    private var sharedShoppingListsID = [String]()
    // Filtered shopping lists
    private lazy var filteredShoppingLists = shoppingLists
    
    // MARK: - Main
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Table view delegate
        shoppingListsTableView.delegate = self
        // Table view data source
        shoppingListsTableView.dataSource = self
        // Search bar delegate
        searchBar.delegate = self
        
        // Estimated row height
        shoppingListsTableView.estimatedRowHeight = 80
        // Row height: automatic dimension
        shoppingListsTableView.rowHeight = UITableView.automaticDimension
                
        // Initialize the shopping lists collection reference
        //shoppingListsCollectionReference = Firestore.firestore().collection(FirebaseCollection.ShoppingLists.collectionName)
        
        // Set color for swipe action icons
        UIImageView.appearance(whenContainedInInstancesOf: [UITableView.self]).tintColor = UIColor(red: 0.00, green: 0.53, blue: 1.00, alpha: 1.0)
        // Set tab bar item text
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .selected)
        
        
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 13)!]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = attributes
        
                    
        
        // Show progress
        //showLoadingPersonalShoppingListsHUD(for_seconds: 2)
        
        getPersonalShoppingLists()
        //getSharedShoppingLists()
        
        // Search
        //definesPresentationContext = true
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        handle = Auth.auth().addStateDidChangeListener({ (auth, user) in
//            // Check if the user is logged in
//            if user == nil {
//                self.dismiss(animated: true, completion: nil)
//            } else { // User logged in
//                self.getPersonalShoppingLists()
//            }
//        })
//    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredShoppingLists = shoppingLists.filter { (shoppingList: ShoppingList) -> Bool in
            return shoppingList.name.lowercased().contains(searchText.lowercased())
        }
        shoppingListsTableView.reloadData()
    }
    
    func setListener(groupID: String) {
        // Snapshot listner for groups collection and order by date of creation in descending order (most recent)
        
        var groupsListener = Firestore.firestore().collection(FirebaseCollection.Groups.collectionName).document(groupID).collection(groupID).addSnapshotListener { (documentSnapshot, error) in
            if let error = error {
                debugPrint("Error fetching documents: \(error.localizedDescription)")
            } else {
                
                
                documentSnapshot?.documentChanges.forEach({ (change) in
                    let data = change.document.data()
                    let group = Group.init(data: data)
                    
                    print(data)
                    
//                    switch change.type {
//                    case .added:
//                        self.onDocumentAdded(change: change, group: group)
//                    case .modified:
//                        self.onDocumentModified(change: change, group: group)
//                    case .removed:
//                        self.onDocumentRemoved(change: change)
//                    @unknown default:
//                        fatalError()
//                    }
                })
            }
        }
    }
        
    // MARK: - Utilities
    // Get current user shopping lists
    func getPersonalShoppingLists() {
        // Snapshot listner for shopping lists collection and order by date of creation in descending order (most recent)
        guard let currentUserID = Auth.auth().currentUser?.uid else { return }
        shoppingListsListener = Firestore.firestore()
            .collection(FirebaseCollection.Users.collectionName)                        // "users"
            .document(currentUserID)                                                    // "currentUserID"
            .collection(FirebaseCollection.ShoppingLists.collectionName)                // "shopping-lists"
            .order(by: FirebaseCollection.ShoppingLists.timestamp, descending: true)    // order by the most recent
            //.whereField(FirebaseCollection.ShoppingLists.shared, isEqualTo: false)
            //.whereField(FirebaseCollection.ShoppingLists.completed, isEqualTo: false)
            .addSnapshotListener { (snapshot, error) in
                // Check for errors
                if let error = error {
                    debugPrint("Error fetching documents \(error.localizedDescription)")
                } else {
                    snapshot?.documentChanges.forEach({ (change) in
                        let data = change.document.data()
                        let shoppingList = ShoppingList.init(data: data)

                        switch change.type {
                        case .added:
                            self.onDocumentAdded(change: change, shoppingList: shoppingList)
                        case .modified:
                            self.onDocumentModified(change: change, shoppingList: shoppingList)
                        case .removed:
                            self.onDocumentRemoved(change: change)
                        @unknown default:
                            fatalError()
                        }
                    })
                    // Check if the user has no shopping lists
                    if self.shoppingLists.count == 0 {
                        self.noShoppingListsInfoMessage.isHidden = false
                        self.searchBar.isHidden = true
                    } else { // The user have shopping lists
                        self.noShoppingListsInfoMessage.isHidden = true
                        self.searchBar.isHidden = false
                    }
                }
        }
    }

    // MARK: - Setup UI

    // MARK: - Navigation
    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // IBActions
    
    // Add new shopping list button action
    @IBAction func addNewShoppingListButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Storyboard.newShoppingListViewController, sender: nil)
    }
    
    // Saved personal shopping lists button action
    @IBAction func savedPersonalShoppingListsButtonWasTapped(_ sender: Any) {
        performSegue(withIdentifier: Segue.Storyboard.savedPersonalShoppingLists, sender: nil)
    }
    
}

// MARK: - Extensions

extension ShoppingListsViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    
    // Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewCell.numberOfSections
    }
    
    // Number of rows in section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredShoppingLists.count > 0 {
            return filteredShoppingLists.count
        }
        return shoppingLists.count
    }
    
    
    // Height for row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCell.height)
    }
    
    // Cell creation
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.ShoppingLists.identifier, for: indexPath) as? ShoppingListTableViewCell {
//            cell.configureCell(shoppingList: shoppingLists[indexPath.row])
//            return cell
//        } else {
//            return UITableViewCell()
//        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.ShoppingLists.identifier, for: indexPath) as! ShoppingListTableViewCell
        //let shoppingList: ShoppingList
        if filteredShoppingLists.count > 0 {
            shoppingList = filteredShoppingLists[indexPath.row]
        } else {
            shoppingList = shoppingLists[indexPath.row]
        }
        cell.configureCell(shoppingList: shoppingList)
        return cell
    }
    
    /*
     // Override to support conditional editing of the table view.
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    
    // Override to support rearranging the table view.
    func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) { }
    
    
    // Override to support conditional rearranging of the table view.
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    
    
    // MARK: - Navigation
    
    /*
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     if let itemsViewController = segue.destination as? ItemsListTableViewController {
     itemsViewController.initItems(shoppingList: sender as! ShoppingList)
     }
     }
     */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Segue.Storyboard.addItemViewController, sender: shoppingLists[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.Storyboard.addItemViewController {
            if let destinationViewController = segue.destination as? AddItemViewController {
                if let shoppingList = sender as? ShoppingList {
                    destinationViewController.shoppingList = shoppingList
                }
            }
        } else if segue.identifier == Segue.Storyboard.selectGroupViewController {
            if let destinationViewController = segue.destination as? SelectGroupViewController {
                if let shoppingListsID = sender as? [String] {
                    self.shoppingListsID.removeAll()
                    destinationViewController.shoppingListsID = shoppingListsID
                }
            }
        }
    }
    
    // MARK: - Table view swipe actions
    
    // Leading Swipe action
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let width: CGFloat = 25
        let height: CGFloat = 25
        
        // Done action: DA RIFARE
        let done = UIContextualAction(style: .normal, title: nil) { (action, view, nil) in
            // Get user id
            guard let userID = Auth.auth().currentUser?.uid else { return }
            self.shoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.shoppingLists[indexPath.row].documentID)
            // Set selected shopping list as complete
            self.shoppingListsService.setAsCompleted(completed: true, shoppingList: self.shoppingLists[indexPath.row], shoppingListReference: self.shoppingListReference)
            // Remove selected shopping list from the shopping list array
            self.shoppingLists.remove(at: indexPath.row)
            // Delete selected shopping list from the table view
            self.shoppingListsTableView.deleteRows(at: [indexPath], with: .fade)
            self.shoppingListsTableView.reloadData()
        }
        
        let doneImage = UIGraphicsImageRenderer(size: CGSize(width: width, height: height)).image { (context) in
            context.stroke(CGRect(x: 0, y: 0, width: width, height: height), blendMode: .clear)
            ItemActionIcon.drawDoneActionCanvas(frame: CGRect(x: 0, y: 0, width: width, height: height), resizing: .aspectFit)
        }
        done.image = doneImage
        done.backgroundColor = .white
        
        let config = UISwipeActionsConfiguration(actions: [done])
        config.performsFirstActionWithFullSwipe = false
        self.shoppingListsTableView.isEditing = false
        
        return config
    }
    
    // Trailing swipe action
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let width: CGFloat = 23
        let height: CGFloat = 23
        
        // Add to preferred action: DA RIFARE
        let addToPreferred = UIContextualAction(style: .normal, title: nil) { (action, view, nil) in
            // Get user id
            guard let userID = Auth.auth().currentUser?.uid else { return }
            self.shoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.shoppingLists[indexPath.row].documentID)
            // Set selected shopping list as complete
            self.shoppingListsService.setAsSaved(saved: true, shoppingList: self.shoppingLists[indexPath.row], shoppingListReference: self.shoppingListReference)
        }
        
        let addToPreferredImage = UIGraphicsImageRenderer(size: CGSize(width: width, height: height)).image { context in
            context.fill(CGRect(origin: .zero, size: CGSize(width: width, height: height)), blendMode: .clear)
            ItemActionIcon.drawAddToPreferredActionCanvasNotActive(frame: CGRect(x: 0, y: 0, width: width, height: height))
        }
        addToPreferred.image = addToPreferredImage
        addToPreferred.backgroundColor = .white
                
        let config = UISwipeActionsConfiguration(actions: [addToPreferred])
        config.performsFirstActionWithFullSwipe = false
        shoppingListsTableView.isEditing = false
        
        return config
    }
    
    func onDocumentAdded(change: DocumentChange, shoppingList: ShoppingList) {
        let newIndex = Int(change.newIndex)
        shoppingLists.insert(shoppingList, at: newIndex)
        shoppingListsTableView.insertRows(at: [IndexPath(row: newIndex, section: 0)], with: .automatic)
    }
    
    func onDocumentModified(change: DocumentChange, shoppingList: ShoppingList) {
        // Shopping list changed but remaind in the same position
        if change.newIndex == change.oldIndex {
            let index = Int(change.newIndex)
            shoppingLists[index] = shoppingList
            shoppingListsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        } else { // Item changed and changed position
            let oldIndex = Int(change.oldIndex)
            let newIndex = Int(change.newIndex)
            shoppingLists.remove(at: oldIndex)
            shoppingLists.insert(shoppingList, at: newIndex)
            shoppingListsTableView.moveRow(at: IndexPath(row: oldIndex, section: 0), to: IndexPath(row: newIndex, section: 0))
        }
    }
    
    func onDocumentRemoved(change: DocumentChange) {
        let oldIndex = Int(change.oldIndex)
        shoppingLists.remove(at: oldIndex)
        shoppingListsTableView.deleteRows(at: [IndexPath(row: oldIndex, section: 0)], with: .left)
    }
}

extension ShoppingListsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredShoppingLists = shoppingLists.filter { (shoppingList: ShoppingList) -> Bool in
            return shoppingList.name.lowercased().contains(searchText.lowercased())
        }
        shoppingListsTableView.reloadData()
    }
}
