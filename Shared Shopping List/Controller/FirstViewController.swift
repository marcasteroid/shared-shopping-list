//
//  CreateUserViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

// Dummy user: use it just for test only
let user = User(username: "gatto_gastone", email: "gastone.gatto@gmail.com", password: "1234567890", uid: 1234)

class CreateUserViewController: UIViewController {

    let cu = CreateUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //CreateUser.instance.createUser(user: user)
        cu.createUser(user: user)
        
    }


}

