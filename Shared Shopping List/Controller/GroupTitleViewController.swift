//
//  GroupTitleViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/07/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class GroupTitleViewController: UIViewController {

    
    // MARK: - IBOutles
    // Group name
    @IBOutlet weak var groupNameTextField: UITextField!
    // Users group table view
    @IBOutlet weak var usersGroupTableView: UITableView!
    
    
    // MARK: - Variables
    
    // Users ids
    var userIds = [String]()
    // User
    var users = [UserGroup]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usersGroupTableView.delegate = self
        usersGroupTableView.dataSource = self
    }
    
    // MARK: - IBActions
    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Utilities
    
    // Create personal group
    func createPersonalGroup(withName name: String, forMembers members: [String], handler: @escaping (_ groupCreated: Bool) -> ()) {
        // Get the user id
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        // Firestore transaction
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Group reference
            let groupReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.Users.groups).document()
            
            transaction.setData([
                FirebaseCollection.Groups.adminID: userID,
                FirebaseCollection.Groups.name: name,
                FirebaseCollection.Groups.membersCount: members.count,
                FirebaseCollection.Groups.members: members,
                FirebaseCollection.Groups.timestamp: FieldValue.serverTimestamp(),
                FirebaseCollection.Groups.shoppingListsID: []], forDocument: groupReference)
            
            let sharedGroupReferenceId = groupReference.documentID
            
//            let  sharedGroupReference = Firestore.firestore().collection(FirebaseCollection.SharedShoppingLists.collectionName).document(sharedGroupReferenceId)
//            
//            transaction.setData([
//                // The name of shared shopping list is equale to the id of the group
//                FirebaseCollection.Groups.name: sharedGroupReferenceId,
//                FirebaseCollection.Groups.members: members
//            ], forDocument: sharedGroupReference)
            
            self.createMemberGroup(withName: sharedGroupReferenceId, forMembers: members) { (groupCreated) in
                if groupCreated { print("OK") } else { print("NOT OK") }
            }
            
            // Create GLOBAL Groups collection
            let globalGroupReference = Firestore.firestore().collection(FirebaseCollection.Groups.collectionName).document(sharedGroupReferenceId).collection(sharedGroupReferenceId).document(sharedGroupReferenceId)
            
            transaction.setData([
                FirebaseCollection.Groups.label: name,
                FirebaseCollection.Groups.adminID: userID,
                FirebaseCollection.Groups.name: sharedGroupReferenceId,
                FirebaseCollection.Groups.membersCount: members.count,
                FirebaseCollection.Groups.members: members,
                FirebaseCollection.Groups.timestamp: FieldValue.serverTimestamp(),
                FirebaseCollection.Groups.shoppingListsID: []], forDocument: globalGroupReference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            }
        }
        handler(true)
    }
    
    // Create member group
    func createMemberGroup(withName name: String, forMembers members: [String], handler: @escaping (_ groupCreated: Bool) -> ()) {
        // Get admin user id
        guard let adminUserID = Auth.auth().currentUser?.uid else { return }
        
        // Firestore transaction
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Group reference
            let groupReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(members[0]).collection(FirebaseCollection.Users.groups).document(name)
            
            transaction.setData([
                FirebaseCollection.Groups.adminID: adminUserID,
                FirebaseCollection.Groups.name: name,
                FirebaseCollection.Groups.membersCount: members.count,
                FirebaseCollection.Groups.members: members,
                FirebaseCollection.Groups.timestamp: FieldValue.serverTimestamp(),
                FirebaseCollection.Groups.shoppingListsID: []], forDocument: groupReference)
                        
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            }
        }
        handler(true)
    }
    
    // Get users ids
    fileprivate func getUsersID(usersIds: [String], currentUserID: String) -> [String] {
        return usersIds.filter { $0 != currentUserID }
    }
    
    // MARK: - IBActions
    
    @IBAction func createGroupButtonWasTapped(_ sender: Any) {
        guard let groupName = groupNameTextField.text, groupName.isNotEmpty else {
            handleGroupCreationError(forError: GroupCreation.CreateNewGroup.Error.groupNameTextFieldError)
            return
        }
        
        // Create personal group
        createPersonalGroup(withName: groupName, forMembers: userIds) { (groupCreated) in
            if groupCreated {
                
                self.groupNameTextField.text = Common.Value.empty
                // Present shopping lists view controller
                let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
                let tabBarViewController = storyboard.instantiateViewController(withIdentifier: ViewController.tabBarController)
                self.present(tabBarViewController, animated: true, completion: nil)
                
                /*
                self.createMemberGroup(withName: groupName, forMembers: self.userIds) { (groupCreated) in
                    if groupCreated {
                        self.groupNameTextField.text = Common.Value.empty
                        // Present shopping lists view controller
                        let storyboard = UIStoryboard(name: Segue.Storyboard.main, bundle: nil)
                        let tabBarViewController = storyboard.instantiateViewController(withIdentifier: ViewController.tabBarController)
                        self.present(tabBarViewController, animated: true, completion: nil)
                    }
                    else {
                        print("Error creating member group")
                    }
                }
                */
            } else {
                print("Error creating group")
            }
        }
    }
}


extension GroupTitleViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.UsersGroupListCell.identifier, for: indexPath) as? UsersGroupTableViewCell else { return UsersGroupTableViewCell() }
        let userGroup = users[indexPath.row]
        
        cell.configureCell(userGroup: userGroup)
        // Set cell background to clear
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        return cell
    }
    
    
}
