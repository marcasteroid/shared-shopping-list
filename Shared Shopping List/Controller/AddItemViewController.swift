//
//  AddItemViewController.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 19/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit
import Firebase

class AddItemViewController: UIViewController {

    // MARK: - IBOutlets
    // Items table view
    @IBOutlet weak var tableView: UITableView!
    // Add item text field
    @IBOutlet weak var addItemTextField: UITextField!
    // Shopping list name label
    @IBOutlet weak var shoppingListName: UILabel!
    // Header view
    @IBOutlet weak var headerView: UIView!
    // Bottom view
    @IBOutlet weak var bottomView: UIView!
    // Add item button
    @IBOutlet weak var addItemButton: UIButton!
    
    
    // MARK: - Properties
    
    // Shopping list
    var shoppingList: ShoppingList!
    // Shopping list document reference
    var shoppingListReference: DocumentReference!
    
    // Items
    // Array of items
    var items = [Item]()
    // Item listener
    var itemListener: ListenerRegistration!
    // Item service
    private var itemService = ItemService()
    // Item document reference
    private var itemReference: DocumentReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup view
        setupView()
        
        //bottomView.bindToKeyboard()
        
        // Table view delegate
        tableView.delegate = self
        // Table view datasource
        tableView.dataSource = self

        // Estimate table view row height
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableView.automaticDimension
        
        shoppingListName.text = shoppingList.name.capitalizeFirstLetter()
        
        // Get user id
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        shoppingListReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID)
        //shoppingListReference = Firestore.firestore().collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID)
        
        addItemTextField.placeholder = "add items to #" + shoppingList.name.replacingOccurrences(of: " ", with: "-")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Get user id
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        // Firestore.firestore().collection(FirebaseCollection.ShoppingLists.collectionName).document(self.shoppingList.documentID)
        itemListener = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID)
            .collection(FirebaseCollection.Items.collectionName)
            .order(by: FirebaseCollection.Items.timestamp, descending: false)
            .addSnapshotListener({ (snapshot, error) in
                guard let snapshot = snapshot else {
                    debugPrint("Error fetching items: \(String.init(describing: error?.localizedDescription))")
                    return
                }
                self.items.removeAll()
                self.items = Item.parseData(snapshot: snapshot)
                self.tableView.reloadData()
            })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if itemListener != nil {
            itemListener.remove()
        }
        items.removeAll()
        tableView.reloadData()
    }

    // MARK: - Setup view
    
    // Setup view
    func setupView() {
        view.bindToKeyboard()
        // Hide keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(AddItemViewController.handleTap))
        view.addGestureRecognizer(tap)
        
        
        //tap.cancelsTouchesInView = false
        
        /*
        // Remove tap gesture from bottom view
        self.bottomView.removeGestureRecognizer(tap)
        // Remove tap gesture from
        self.addItemButton.removeGestureRecognizer(tap)
        */
        
        bottomView.addSubview(addItemButton)
        
        bottomView.gestureRecognizers?.forEach(bottomView.removeGestureRecognizer)
        
        if let tapGestures = addItemButton.gestureRecognizers {
            for tapGesture in tapGestures {
                addItemButton.removeGestureRecognizer(tapGesture)
            }
        }
        
        
        // Listen for keyboard events
        // Keyboard will show
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        // Keyboard will hide
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Keyboard will change frame
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // Keyboard will change
    @objc func keyboardWillChange(notification: Notification) {
        // Keyboard rectangle
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillShowNotification || notification.name == UIResponder.keyboardWillChangeFrameNotification {
            // Hide the header view
            self.headerView.isHidden = true
            // Set frame y origin
            view.frame.origin.y = -keyboardRect.height
        } else {
            // Set header view visible
            self.headerView.isHidden = false
            // Reset frame y origin
            view.frame.origin.y = 0
        }
    }
    
    // Remove notifications
    deinit {
        // Stop listening for keyboard hide/show events
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    // MARK: - Gesture recognizer
    
    // Handle tap
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    // MARK: - Navigation

    @IBAction func backButtonWasTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: - IBActions
    
    @IBAction func addItem(_ sender: Any) {
        // Check if the item name text field is not empty
        guard let itemName = addItemTextField.text, itemName.isNotEmpty else {
            handleShoppingListCreationError(forError: ShoppingListCreation.CreateNewShoppingList.Error.shoppingListItemNameTextFieldError)
            return
        }
        
        // Get user id
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        // Firestore transaction
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            let shoppingListDocument: DocumentSnapshot
            
            do {
                try shoppingListDocument = transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.shoppingList.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            
            // Get the number of items in the shopping list
            guard let oldNumberOfItems = shoppingListDocument.data()?[FirebaseCollection.ShoppingLists.numberOfItems] as? Int else { return nil }
            
            transaction.updateData([FirebaseCollection.ShoppingLists.numberOfItems : oldNumberOfItems + 1], forDocument: self.shoppingListReference)
            
            let newItemReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.shoppingList.documentID).collection(FirebaseCollection.Items.collectionName).document()
            
            transaction.setData([
                FirebaseCollection.Items.name: itemName,
                FirebaseCollection.Items.timestamp: FieldValue.serverTimestamp(),
                FirebaseCollection.Items.shoppingListID: String(self.shoppingList.documentID),
                FirebaseCollection.Items.done: false], forDocument: newItemReference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(String.init(describing: error.localizedDescription))")
            } else {
                self.addItemTextField.clear()
                self.addItemTextField.resignFirstResponder()
            }
        }
    }
}


extension AddItemViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableViewCell.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let itemCell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.ItemList.identifier , for: indexPath) as? ItemsListTableViewCell {
            itemCell.configureCell(item: items[indexPath.row])
            itemCell.delegate = self
            return itemCell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(TableViewCell.height)
    }
}

extension AddItemViewController: TableViewCellDelegate {
    
    func doubleTapDetected(in cell: ItemsListTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            // Get user id
            guard let userID = Auth.auth().currentUser?.uid else { return }
            
            self.itemReference = Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(self.shoppingList.documentID).collection(FirebaseCollection.Items.collectionName).document(self.items[indexPath.row].documentID)
            
            guard let done = items[indexPath.row].done else { return }
            
            if done {
                cell.removeStrikeThrough(item: items[indexPath.row])
                cell.restoreItemLabelAlpha()
                itemService.setAsDone(done: false, shoppingList: shoppingList, item: items[indexPath.row], itemReference: itemReference)
            } else {
                cell.strikeThrough(item: items[indexPath.row])
                cell.lowerItemLabelAlpha()
                itemService.setAsDone(done: true, shoppingList: shoppingList, item: items[indexPath.row], itemReference: itemReference)
            }
        }
    }
    
    func singleTapDetected(in cell: ItemsListTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            print("Singl tap: \(indexPath)")
        }
    }
}
