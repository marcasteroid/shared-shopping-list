//
//  ShoppingListTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 08/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

class ShoppingListTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    // Shopping list name
    @IBOutlet weak var shoppingListNameLabel: UILabel!
    // Number of items
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    // Items
    @IBOutlet weak var itemsLabel: UILabel!
    // Date created
    @IBOutlet weak var dateCreatedLabel: UILabel!
    // Saved info
    @IBOutlet weak var savedInfoCircle: CustomImage!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(shoppingList: ShoppingList) {
        updateView(shoppingList: shoppingList)
    }
    
    func dateFormatter(shoppingList: ShoppingList) -> String {
        let formatter = DateFormatter()
        // Format date as: number - month - hour - minutes
        formatter.dateFormat = Formatter.Date.dayMonthHMin
        let timestamp = formatter.string(from: shoppingList.timestamp.dateValue())
        return timestamp
    }
    
    fileprivate func updateView(shoppingList: ShoppingList) {
        shoppingListNameLabel.text = shoppingList.name
        numberOfItemsLabel.text = String(shoppingList.numberOfItems!)
        dateCreatedLabel.text = dateFormatter(shoppingList: shoppingList)
        if shoppingList.completed {
            print("Complete")
        }
        // Check if the shopping list is saved
        if shoppingList.saved {
            savedInfoCircle.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.4980392157, blue: 0.5098039216, alpha: 1)
        } else {
            savedInfoCircle.borderWidth = 1
            savedInfoCircle.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            savedInfoCircle.borderColor = #colorLiteral(red: 0.9215686275, green: 0.4980392157, blue: 0.5098039216, alpha: 1)
        }
        // Check the number of items in the shopping list
        if shoppingList.numberOfItems! == 1 {
            // Set the item label to "item"
            itemsLabel.text = Message.ShoppingLists.singleItem
        } else {
            // Set the item label to "items"
            itemsLabel.text = Message.ShoppingLists.moreItems
        }
    }
}
