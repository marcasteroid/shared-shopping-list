//
//  UsersGroupTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 20/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class UsersGroupTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    // Contact profile image
    @IBOutlet weak private var contactProfileImage: CustomImage!
    // Contact fullname
    @IBOutlet weak var contactFullName: UILabel!
    // First letter
    @IBOutlet weak private var firstLetter: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Update View
    
    // Configure cell
    func configureCell(userGroup: UserGroup) {
        // Set contact profile image
        self.contactProfileImage.image = userGroup.image
        // Set contact fullname
        self.contactFullName.text = userGroup.fullname
        // Check if the contact has a profile image
        let firstCharacter = String(userGroup.fullname.first!)
        self.firstLetter.text = firstCharacter
    }

}

/*
class AddressBookContactTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlets
    
    // Contact profile image
    @IBOutlet weak private var contactProfileImage: CustomImage!
    // Contact fullname
    @IBOutlet weak var contactFullName: UILabel!
    // Check image
    @IBOutlet weak private var checkImage: UIImageView!
    // First letter
    @IBOutlet weak private var firstLetter: UILabel!
    
    // MARK: - Variables
    
    // Selecting table view cell
    fileprivate var selecting: Bool = false
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            if selecting == false {
                print("Selected")
                //self.isUserInteractionEnabled = false
                selecting = true
            } else {
                print("Not selected")
                //self.isUserInteractionEnabled = true
                selecting = false
            }
        }
    }
    
    // MARK: - Update View
    
    // Configure cell
    func configureCell(contact: AddressBookContact) {
        // Set contact profile image
        self.contactProfileImage.image = contact.image
        // Set contact fullname
        self.contactFullName.text = contact.firstName + contact.lastName
        self.contactFullName.makeFontStyleRegularAndBold(headText: contact.firstName, tailText: contact.lastName)
        // Check if the contact has a profile image
        if contact.hasContactImage {
            self.firstLetter.text = Common.Value.empty
            self.contactProfileImage.borderWidth = 0.0
        } else {
            let firstCharacter = String(contact.firstName.first!)
            self.firstLetter.text = firstCharacter
        }
    }
}
*/
