//
//  AddressBookContactTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 08/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class AddressBookContactTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    // Contact profile image
    @IBOutlet weak private var contactProfileImage: CustomImage!
    // Contact fullname
    @IBOutlet weak var contactFullName: UILabel!
    // Check image
    @IBOutlet weak private var checkImage: UIImageView!
    // First letter
    @IBOutlet weak private var firstLetter: UILabel!

    // MARK: - Variables
    
    // Selecting table view cell
    fileprivate var selecting: Bool = false
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            if selecting == false {
                print("Selected")
                //self.isUserInteractionEnabled = false
                selecting = true
            } else {
                print("Not selected")
                //self.isUserInteractionEnabled = true
                selecting = false
            }
        }
    }
    
    // MARK: - Update View
    
    // Configure cell
    func configureCell(contact: AddressBookContact) {
        // Set contact profile image
        self.contactProfileImage.image = contact.image
        // Set contact fullname
        self.contactFullName.text = contact.firstName + contact.lastName
        self.contactFullName.makeFontStyleRegularAndBold(headText: contact.firstName, tailText: contact.lastName)
        // Check if the contact has a profile image
        if contact.hasContactImage {
            self.firstLetter.text = Common.Value.empty
            self.contactProfileImage.borderWidth = 0.0
        } else {
            let firstCharacter = String(contact.firstName.first!)
            self.firstLetter.text = firstCharacter
        }
    }
}
