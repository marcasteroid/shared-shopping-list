//
//  UserProfileTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    
    // Entry name
    @IBOutlet weak var entryNameLabel: UILabel!
    // Entry image
    @IBOutlet var entryImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Configure cell
    func configureCell(userProfileActionsList: UserProfileAction) {
        updateView(userProfileActionsList: userProfileActionsList)
    }
    
    // Update view
    fileprivate func updateView(userProfileActionsList: UserProfileAction) {
        entryNameLabel.text = userProfileActionsList.name
        entryImage.image = UIImage(named: userProfileActionsList.imageName)
    }

}
