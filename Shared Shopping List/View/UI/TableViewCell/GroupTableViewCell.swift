//
//  GroupTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 14/07/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    // Group name
    @IBOutlet weak var groupNameLabel: UILabel!
    // Number of members
    @IBOutlet weak var numberOfMembersLabel: UILabel!
    // Members
    @IBOutlet weak var membersLabel: UILabel!
    // Date created
    @IBOutlet weak var dateCreatedLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func dateFormatter(group: Group) -> String {
        let formatter = DateFormatter()
        // Format date as: number - month - hour - minutes
        formatter.dateFormat = Formatter.Date.dayMonthHMin
        let timestamp = formatter.string(from: group.timestamp.dateValue())
        return timestamp
    }
    
    func configureCell(group: Group) {
        updateView(group: group)
    }
    
    func updateView(group: Group) {
        groupNameLabel.text = group.label
        numberOfMembersLabel.text = String(group.membersCount!)
        dateCreatedLabel.text = dateFormatter(group: group)
        membersLabel.text = Message.Groups.members
    }

}
