//
//  ItemsListTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 17/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

protocol TableViewCellDelegate: class {
    func doubleTapDetected(in cell: ItemsListTableViewCell)
    func singleTapDetected(in cell: ItemsListTableViewCell)
}

class ItemsListTableViewCell: UITableViewCell, MultiTappable {

    // MARK: - IBOutlets
    @IBOutlet weak var itemName: UILabel!
    
    // MARK: - Properties
    weak var multiTapDelegate: MultiTappableDelegate?
    lazy var tapCounter = ThreadSafeValue(value: 0)
    weak var delegate: TableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initMultiTap()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(item: Item) {
        updateView(item: item)
    }
    
    func updateView(item: Item) {
        itemName.text = item.name
        self.isDone(item: item)
    }
    
    func strikeThrough(item: Item) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: itemName.text!)
        attributeString.addAttribute(.strikethroughStyle, value: 1, range: NSRange(location: 0, length: itemName.text!.count))
        itemName.attributedText = attributeString
    }
    
    func lowerItemLabelAlpha() {
        itemName.alpha = 0.5
    }
    
    func restoreItemLabelAlpha() {
        itemName.alpha = 1.0
    }
    
    func removeStrikeThrough(item: Item) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: itemName.text!)
        itemName.attributedText = attributeString
    }
    
    func isDone(item: Item){
        if item.done {
            self.strikeThrough(item: item)
            self.lowerItemLabelAlpha()
            //deinitMultiTap()
        }
    }
}

extension ItemsListTableViewCell: MultiTappableDelegate {
    
    func doubleTapDetected(in view: MultiTappable) {
        self.delegate?.doubleTapDetected(in: self)
    }
    
    func singleTapDetected(in view: MultiTappable) {
        self.delegate?.singleTapDetected(in: self)
    }
}
