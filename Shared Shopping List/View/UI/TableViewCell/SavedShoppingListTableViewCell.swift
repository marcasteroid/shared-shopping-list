//
//  SavedShoppingListTableViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 09/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class SavedShoppingListTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    // Shopping list name
    @IBOutlet weak var shoppingListNameLabel: UILabel!
    // Number of items
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    // Items
    @IBOutlet weak var itemsLabel: UILabel!
    // Date saved
    @IBOutlet weak var dateSavedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureCell(savedShoppingList: SavedShoppingList) {
        updateView(savedShoppingList: savedShoppingList)
    }
    
    private func dateFormatter(savedShoppingList: SavedShoppingList) -> String {
        let formatter = DateFormatter()
        // Format date as: number - month - hour - minutes
        formatter.dateFormat = Formatter.Date.dayMonthHMin
        let timestamp = formatter.string(from: savedShoppingList.dateComplete.dateValue())
        return timestamp
    }
    
    private func updateView(savedShoppingList: SavedShoppingList) {
        shoppingListNameLabel.text = savedShoppingList.name
        numberOfItemsLabel.text = String(savedShoppingList.numberOfItems!)
        dateSavedLabel.text = dateFormatter(savedShoppingList: savedShoppingList)
        // Check the number of items in shopping list
        if savedShoppingList.numberOfItems! == 0 || savedShoppingList.numberOfItems! == 1 {
            // Set item label to "item"
            itemsLabel.text = Message.ShoppingLists.singleItem
        } else {
            // Set item label to items
            itemsLabel.text = Message.ShoppingLists.moreItems
        }
    }

}
