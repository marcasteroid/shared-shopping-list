//
//  AddItemButtonDraw.swift
//  Swipe Actions
//
//  Created by Marco Margarucci on 04/10/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class AddItemButtonDraw: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        NavigationBarButton.drawAddActionCanvas(frame: self.bounds, resizing: .aspectFit)
    }
}
