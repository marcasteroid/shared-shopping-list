//
//  DoneActionDraw.swift
//  Swipe Actions
//
//  Created by Marco Margarucci on 02/10/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class DoneActionDraw: UIView {
    
    override func draw(_ rect: CGRect) {
        ItemActionIcon.drawDoneActionCanvas(frame: self.bounds, resizing: .aspectFit)
    }
}
