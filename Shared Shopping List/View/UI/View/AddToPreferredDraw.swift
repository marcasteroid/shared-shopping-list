//
//  AddToPreferredDraw.swift
//  Swipe Actions
//
//  Created by Marco Margarucci on 03/10/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class AddToPreferredDraw: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        ItemActionIcon.drawAddToPreferredActionCanvasActive(frame: self.bounds, resizing: .aspectFit)
    }
}
