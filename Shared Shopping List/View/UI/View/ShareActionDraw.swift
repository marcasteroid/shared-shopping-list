//
//  ShareActionDraw.swift
//  Swipe Actions
//
//  Created by Marco Margarucci on 02/10/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class ShareActionDraw: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        ItemActionIcon.drawShareActionCanvas(frame: self.bounds, resizing: .aspectFit)
    }

}
