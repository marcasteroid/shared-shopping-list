//
//  CustomView.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomView: UIView {
    
    //Corner radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
