//
//  CustomImage.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 24/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class CustomImage: UIImageView {
    
    //Corner radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    // Border width
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    // Border color
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    // Shadow color
    @IBInspectable var shadowColor: UIColor = .clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    // Shadow offset
    @IBInspectable var shadowOffset = CGSize(width: 0, height: 0) {
        didSet {
            self.layer.shadowOffset = CGSize(width: 5, height: 5)
        }
    }
    
    // Shadow radius
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    // Shadow opacity
    @IBInspectable var shadowOpacity: Float = 0 {
        didSet {
            self.layer.shadowOpacity = shadowOpacity
        }
    }
}
