//
//  UserGroupCollectionViewCell.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import UIKit

class UserGroupCollectionViewCell: UICollectionViewCell {
    
    // MARK: - IBOutlets
    @IBOutlet weak var groupUserImage: CustomImage!
    @IBOutlet weak var groupUserFullNameLabel: UILabel!
    @IBOutlet weak var groupUserFirstNameLetterLabel: UILabel!
    
    func updateView(userGroup: UserGroup) {
        groupUserFullNameLabel.text = userGroup.fullname
        guard let userImage = userGroup.image else {
            groupUserFirstNameLetterLabel.isHidden = false
            groupUserFirstNameLetterLabel.text = String(userGroup.fullname.first!)
            return
        }
        groupUserImage.image = userImage
        groupUserFirstNameLetterLabel.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        groupUserImage.image = nil
    }
}
