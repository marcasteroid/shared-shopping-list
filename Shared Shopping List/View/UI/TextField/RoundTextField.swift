//
//  RoundTextField.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 20/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import UIKit

@IBDesignable
class RoundTextField: UITextField {

    //Corner radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    // Border width
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    // Border color
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
