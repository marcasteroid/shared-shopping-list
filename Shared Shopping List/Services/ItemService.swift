//
//  ItemService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 28/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase


class ItemService {
    // Instance
    static let instance = ItemService()

    // Set as done
    public func setAsDone(done: Bool, shoppingList: ShoppingList, item: Item, itemReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID).collection(FirebaseCollection.Items.collectionName).document(item.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Get the value of "shared" field
            transaction.updateData([FirebaseCollection.Items.done: done], forDocument: itemReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
}
