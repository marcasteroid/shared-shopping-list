//
//  AddressBookDataService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 09/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

class AddressBookDataService {
    // Class instance
    static let instance = AddressBookDataService()
    
    // Array of address book contacts
    public var addressBookContacts = [AddressBookContact]()
    
    // Get address book contacts items
    public func getAddressBookContats() -> [AddressBookContact] {
        return addressBookContacts
    }
}
