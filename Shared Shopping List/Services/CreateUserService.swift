//
//  CreateUserService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import UIKit

class CreateUser {
    //static let instance = CreateUser()
    
    // MARK: - Create user
    
    // Create a user in Firestore
    public func createUser(user: User, onSuccess success: @escaping (_ userInfo: String?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: user.email, password: user.password) { (authResult, error) in
            // Check if there is an error creating the user
            if let error = error { failure(error) }
            // Get user info
            guard let createdUser = authResult?.user else { return }
            success(createdUser.uid)
            
            // Create a username
            let changeRequest = authResult?.user.createProfileChangeRequest()
            changeRequest?.displayName = user.username
            changeRequest?.commitChanges(completion: { (error) in
                if let error = error { failure(error) }
            })
            
            // Create the "users" collection
            guard let userID = authResult?.user.uid else { return }
            Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).setData([
                // Username
                FirebaseCollection.Users.username: user.username,
                // Email address
                FirebaseCollection.Users.emailAddress: user.email,
                // Phone number
                FirebaseCollection.Users.phoneNumber: user.phoneNumber,
                // Phone number with prefix
                FirebaseCollection.Users.phoneNumberWithPrefix: user.phoneNumberWithPrefix,
                // Date created
                FirebaseCollection.Users.dateCreated: FieldValue.serverTimestamp(),
                // Image URL
                FirebaseCollection.Users.imageURL: Common.Value.empty,
                // ID
                FirebaseCollection.Users.id: Common.Value.empty,
                // First name
                FirebaseCollection.Users.firstName: Common.Value.empty,
                // Last name
                FirebaseCollection.Users.lastName: Common.Value.empty,
                // Age
                FirebaseCollection.Users.age: Common.Value.empty], completion: { (error) in
                // Check if there is an error
                if let error = error {
                    failure(error)
                } else {
                    success(nil)
                }
            })
        }
    }
}
