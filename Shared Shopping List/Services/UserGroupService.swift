//
//  UserGroupService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 07/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

class UserGroupService {
    static let instance = UserGroupService()
    
    var usersGroup = [UserGroup]()
    
    func getUsersGroup() -> [UserGroup] {
        return usersGroup
    }
}
