//
//  DataService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

// ADD DESCRIPTION

import Foundation
import Firebase

class DataService {
    // Instance
    static let instance = DataService()
    
    // MARK: - Properties
    
    // Base URL
    private var _REF_BASE = FirebaseDatabase.db_base
    // Users
    private var _REF_USERS = FirebaseDatabase.db_base.child(FirebaseCollection.Users.collectionName)
    // Groups
    private var _REF_GROUPS = FirebaseDatabase.db_base.child(FirebaseCollection.Users.groups)
    // List
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_USERS: DatabaseReference {
        return _REF_USERS
    }
    
    var REF_GROUPS: DatabaseReference {
        return _REF_GROUPS
    }
}
