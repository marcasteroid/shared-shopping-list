//
//  SavedShoppingListsServices.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 10/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class SavedShoppingListsService {
    // Instance
    static let instance = SavedShoppingListsService()
    
    // Array of saved shopping lists
    var savedShoppingLists = [SavedShoppingList]()
    
    
    func getSavedShoppingList() -> [SavedShoppingList] {
        return savedShoppingLists
    }
    
    public func setAsSaved(saved: Bool, savedShoppingList: SavedShoppingList, shoppingListReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(savedShoppingList.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Get the value of "saved" field
            transaction.updateData([FirebaseCollection.ShoppingLists.saved: saved,
                                    FirebaseCollection.ShoppingLists.dateCompleted: FieldValue.serverTimestamp()], forDocument: shoppingListReference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    
    // Set as completed
    public func setAsCompleted(completed: Bool, savedShoppingList: SavedShoppingList, shoppingListReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(savedShoppingList.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Set the value of "complete" field
            transaction.updateData([FirebaseCollection.ShoppingLists.completed: completed], forDocument: shoppingListReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    
    func buyAgain(savedShoppingList: SavedShoppingList, shoppingListReference: DocumentReference) {
        // Get user id
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        var newValue: Bool = false
        
        // Firestore transaction
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            let shoppingListDocument: DocumentSnapshot
            
            do {
                try shoppingListDocument = transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.SavedShoppingLists.collectionName).document(savedShoppingList.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            
            guard let completed = shoppingListDocument.data()?[FirebaseCollection.ShoppingLists.completed] as? Bool else { return nil }
            
            if completed { newValue = false }
            
            transaction.updateData([FirebaseCollection.ShoppingLists.completed: newValue], forDocument: shoppingListReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(String.init(describing: error.localizedDescription))")
            } else {
                print("Transaction OK")
            }
        }
    }
    
}
