//
//  ShoppingListsServices.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 08/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class ShoppingListsService {
    // Instance
    static let instance = ShoppingListsService()
    
    // Array of shopping lists
    var shoppingLists = [ShoppingList]()

    
    func getShoppingList() -> [ShoppingList] {
        return shoppingLists
    }
    
    // Set as completed
    public func setAsCompleted(completed: Bool, shoppingList: ShoppingList, shoppingListReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
                do {
                    try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID))
                } catch let error as NSError {
                    debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                    return nil
                }
                // Get the value of "complete" field
            transaction.updateData([FirebaseCollection.ShoppingLists.completed: completed], forDocument: shoppingListReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    
    // Set as saved
    public func setAsSaved(saved: Bool, shoppingList: ShoppingList, shoppingListReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Get the value of "saved" field
            transaction.updateData([FirebaseCollection.ShoppingLists.saved: saved,
                                    FirebaseCollection.ShoppingLists.dateCompleted: FieldValue.serverTimestamp()], forDocument: shoppingListReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    /*
    // Set as shared
    public func setAsShared(shared: Bool, shoppingList: ShoppingList, shoppingListReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingList.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Get the value of "shared" field
            transaction.updateData([FirebaseCollection.ShoppingLists.shared: shared], forDocument: shoppingListReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    */
    // Set as shared
    public func setAsShared(shared: Bool, shoppingListID: String, shoppingListReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Shopping list document snapshot
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.ShoppingLists.collectionName).document(shoppingListID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Get the value of "shared" field
            transaction.updateData([FirebaseCollection.ShoppingLists.shared: shared], forDocument: shoppingListReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("(Set as shared) Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
}
