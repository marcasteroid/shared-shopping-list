//
//  ActionsService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 02/02/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import UIKit

class UserProfileActionsService {
    static let instance = UserProfileActionsService()
    
    private let userProfileActions = [
        UserProfileAction(name: UserProfileActions.Action.recommend, imageName: UserProfileActions.Image.recommend),
        UserProfileAction(name: UserProfileActions.Action.sendFeedback, imageName: UserProfileActions.Image.sendFeedback),
        UserProfileAction(name: UserProfileActions.Action.review, imageName: UserProfileActions.Image.review),
        UserProfileAction(name: UserProfileActions.Action.notifications, imageName: UserProfileActions.Image.notifications),
        UserProfileAction(name: UserProfileActions.Action.credits, imageName: UserProfileActions.Image.credits),
        UserProfileAction(name: UserProfileActions.Action.settings, imageName: UserProfileActions.Image.settings),
        UserProfileAction(name: UserProfileActions.Action.help, imageName: UserProfileActions.Image.help),
        UserProfileAction(name: UserProfileActions.Action.about, imageName: UserProfileActions.Image.about),
        UserProfileAction(name: UserProfileActions.Action.edit, imageName: UserProfileActions.Image.edit),
        UserProfileAction(name: UserProfileActions.Action.deleteAccount, imageName: UserProfileActions.Image.deleteAccount),
        UserProfileAction(name: UserProfileActions.Action.logout, imageName: UserProfileActions.Image.logout)
    ]
    
    func getActions() -> [UserProfileAction] {
        return userProfileActions
    }
}
