//
//  ItemsListService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 17/01/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation

class ItemsListService {
    static let instance = ItemsListService()
    
    // Array of items
    var items = [Item]()
    
    func getItems() -> [Item] {
        return items
    }
    
    func getItems(withName name: String) -> [Item] {
        return items
    }
    
}
