//
//  NetworkService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 25/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Network

class NetworkService {
    // Instance
    static let instance = NetworkService()
    
    func checkNetwork() {
        let monitor = NWPathMonitor()
        monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("You are online")
            } else {
                print("You are offline")
            }
        }
        
    }
}
