//
//  EditUserProfileService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 24/08/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class EditUserProfileService {
    // Instance
    static let instance = EditUserProfileService()

    public func updateInfo(firstName: String, lastName: String, age: String, phoneNumber: String, phoneNumberWithPrefix: String, usersReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Update fields
            transaction.updateData([FirebaseCollection.Users.firstName : firstName,
                                    FirebaseCollection.Users.lastName : lastName,
                                    FirebaseCollection.Users.age : age,
                                    FirebaseCollection.Users.phoneNumber : phoneNumber,
                                    FirebaseCollection.Users.phoneNumberWithPrefix : phoneNumberWithPrefix], forDocument: usersReference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    
    public func updateProfileImage(url: String, usersReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            do {
                try transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(String.init(describing: error.localizedDescription))")
                return nil
            }
            // Update fields
            transaction.updateData([FirebaseCollection.Users.imageURL: url], forDocument: usersReference)
            
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    
}

