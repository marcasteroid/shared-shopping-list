//
//  StandardLoginService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 28/12/2018.
//  Copyright © 2018 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import UIKit

class StandardLoginService {
    
    // MARK: - Login
    
    // Login with email and password
    public func loginWithEmailAndPassword(email: String, andPassword password: String, onSuccess success: @escaping (_: Any?) -> Void, onFailure failure: @escaping (_ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authDataResult, error) in
            // Check if there is an error
            if let error = error {
                failure(error)
            } else {
                success(nil)
            }
        }
    }
    
}
