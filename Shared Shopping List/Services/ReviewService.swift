//
//  ReviewService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 24/05/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import StoreKit

// Class ReviewService

class ReviewService {
    // Instance
    static let instance = ReviewService()
    
    // MARK: - Properties
    
    // User default
    private let defaults = UserDefaults.standard
    
    // MARK: - Initializers
    
    // Default initializer
    private init() {}
    
    // MARK: - Functions
    
    // Request review
    func requestReview() {
        // Check if the user can request a review
        guard shouldRequestReview else { return }
        SKStoreReviewController.requestReview()
        lastRequest = Date()
    }
    
    // Last review request
    private var lastRequest: Date? {
        get {
            return defaults.value(forKey: Services.ReviewService.key) as? Date
        }
        set {
            defaults.set(newValue, forKey: Services.ReviewService.key)
        }
    }
    
    // Elapsed time since last review: one week
    private var elapsedTime: Date {
        return Calendar.current.date(byAdding: .day, value: Services.ReviewService.ElapsedTime, to: Date())!
    }
    
    // Should request review
    private var shouldRequestReview: Bool {
        if lastRequest == nil {
            return true
        } else if let lastRequest = self.lastRequest, lastRequest < elapsedTime {
            return true
        }
        return false
    }
}
