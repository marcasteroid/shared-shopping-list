//
//  SelectGroupService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 21/09/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth

class SelectGroupService {
    // Instance
    static let instance = SelectGroupService()
    
    var groups = [Group]()
    /*
    public func addShoppingListID(shoppingListsID: [String], group: Group, groupReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Group document snapshot
            let groupDocument: DocumentSnapshot

            do {
                try groupDocument = transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.Groups.collectionName).document(group.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            guard let oldShoppingListsID = groupDocument.data()?[FirebaseCollection.Groups.shoppingListsID] as? [String] else { return nil }
            
            let newShoppingListsID = Array(Set(shoppingListsID + oldShoppingListsID))
            
            transaction.updateData([FirebaseCollection.Groups.shoppingListsID: newShoppingListsID], forDocument: groupReference)
            return nil
        }) { (object, error) in
            if let error = error {
                debugPrint("Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
    */
    public func addShoppingListID(shoppingListsID: [String], group: Group, groupReference: DocumentReference) {
        // Check if the user is logged in
        guard let userID = Auth.auth().currentUser?.uid else { return }
        
        Firestore.firestore().runTransaction({ (transaction, errorPointer) -> Any? in
            // Group document snapshot
            let groupDocument: DocumentSnapshot
            
            do {
                try groupDocument = transaction.getDocument(Firestore.firestore().collection(FirebaseCollection.Users.collectionName).document(userID).collection(FirebaseCollection.Groups.collectionName).document(group.documentID))
            } catch let error as NSError {
                debugPrint("Fetch error: \(error.localizedDescription)")
                return nil
            }
            
            guard let currentShoppingListsID = groupDocument.data()?[FirebaseCollection.Groups.shoppingListsID] as? [String] else { return nil }
            
            var oldShoppingListsID = currentShoppingListsID
            
            if oldShoppingListsID.contains(shoppingListsID[0]) {
                oldShoppingListsID.removeAll()
                return nil
            } else {
                var newShoppingListsID = [String]()
                newShoppingListsID = Array(Set(oldShoppingListsID + shoppingListsID))
                
                transaction.updateData([FirebaseCollection.Groups.shoppingListsID: newShoppingListsID], forDocument: groupReference)
                
                newShoppingListsID.removeAll()
                oldShoppingListsID.removeAll()
                
                return nil
            }
        }) { (object, error) in
            if let error = error {
                debugPrint("(Add shopping list ID) Transaction failed: \(error.localizedDescription)")
            } else {
                print("Transaction OK")
            }
        }
    }
}
