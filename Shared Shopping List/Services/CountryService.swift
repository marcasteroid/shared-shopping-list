//
//  CountryService.swift
//  Shared Shopping List
//
//  Created by Marco Margarucci on 05/03/2019.
//  Copyright © 2019 Marco Margarucci. All rights reserved.
//

import Foundation
import RealmSwift
import Firebase

class CountryService: Object {    

    // Add country to the database
    static func add(country: Country) {
        do {
            // Create Realm instance
            let realm = try Realm()
            // Write operation
            try realm.write {
                // Add an object to the database
                realm.add(country)
                // Commit write
                try realm.commitWrite()
            }
        } catch let error {
            debugPrint("Error adding COUNTRY to Realm: \(error.localizedDescription)")
        }
    }
    
    // Get country code by name
    static func getCountryCode(withName name: String)  -> Results<Country>? {
        do {
            // Create Realm instance
            let realm = try Realm()
            var code = realm.objects(Country.self)
            // NSPredicate: predicate for search query
            let predicate = NSPredicate(format: "name LIKE %@", name)
            code = code.filter(predicate)
            return code
        } catch _ {
            return nil
        }
    }
}
